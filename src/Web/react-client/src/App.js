import React from 'react';
import './App.css';

import { BrowserRouter as Router, Switch, Route, Link, } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap'
import { LinkContainer } from "react-router-bootstrap";

import CoursesList from "./features/courses/CoursesList";
import CourseInfo from "./features/courses/CourseInfo";
import CourseEdit from "./features/courses/CourseEdit";
import CourseAdd from "./features/courses/CourseAdd";

import OrganizationEdit from "./features/organizations/OrganizationEdit";
import OrganizationAdd from "./features/organizations/OrganizationAdd";
import OrganizationList from "./features/organizations/OrganizationList";

import TaskSolve from "./features/tasks/TaskSolve";
import TaskEdit from "./features/tasks/TaskEdit";
import TaskAdd from "./features/tasks/TaskAdd";

import LessonEdit from "./features/lessons/LessonEdit";
import LessonAdd from "./features/lessons/LessonAdd";

import RolesList from "./features/roles/RolesList";
import RolesAdd from "./features/roles/RolesAdd";
import RolesEdit from "./features/roles/RolesEdit";
import { ApplicationPaths } from "./features/auth/ApiAuthorizationConstants";
import ApiAuthorizationRoutes from "./features/auth/ApiAuthorizationRoutes";
import AuthorizeRoute from "./features/auth/AuthorizeRoute";
import { LoginMenu } from "./features/auth/LoginMenu";
import LessonDetails from "./features/lessons/LessonDetails";


function App() {
  return (
      <Router>
        <div className="container">
          <div className="container">
            <Navbar color="light" expand="lg" sticky="top">
              <Navbar.Brand>EduForm</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav"/>
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto" bg="dark">

                  <LinkContainer to="/CoursesList">
                    <NavDropdown.Item>Курсы</NavDropdown.Item>
                  </LinkContainer>

                  <LinkContainer to="/OrganizationsList">
                    <NavDropdown.Item>Организации</NavDropdown.Item>
                  </LinkContainer>

                  <LinkContainer to="/RolesList">
                    <NavDropdown.Item>Роли</NavDropdown.Item>
                  </LinkContainer>

                </Nav>

                <LoginMenu>
                </LoginMenu>

              </Navbar.Collapse>
            </Navbar>
          </div>

          <Switch>

            <AuthorizeRoute path='/CoursesList' component={CoursesList}/>
            <AuthorizeRoute path='/CourseInfo/:id' component={CourseInfo}/>
            <AuthorizeRoute path='/CourseEdit/:id' component={CourseEdit}/>
            <AuthorizeRoute path='/CourseAdd' component={CourseAdd}/>

            <AuthorizeRoute path='/OrganizationEdit/:id' component={OrganizationEdit}/>
            <AuthorizeRoute path='/OrganizationAdd' component={OrganizationAdd}/>
            <AuthorizeRoute path='/OrganizationsList' component={OrganizationList}/>

            <AuthorizeRoute path='/TaskSolve/:id'>
              <TaskSolve/>
            </AuthorizeRoute>
            <AuthorizeRoute path='/TaskEdit/:id'>
              <TaskEdit/>
            </AuthorizeRoute>
            <AuthorizeRoute path="/TaskAdd/:courseId">
              <TaskAdd/>
            </AuthorizeRoute>

            <AuthorizeRoute path='/editLesson/:id'>
              <LessonEdit/>
            </AuthorizeRoute>
            <AuthorizeRoute path='/LessonAdd/:courseId'>
              <LessonAdd/>
            </AuthorizeRoute>
            <AuthorizeRoute path='/LessonDetails/:id'>
              <LessonDetails/>
            </AuthorizeRoute>

            <AuthorizeRoute path='/RolesList' component={RolesList}/>
            <AuthorizeRoute path='/RolesAdd' component={RolesAdd}/>
            <AuthorizeRoute path='/RolesEdit/:id' component={RolesEdit}/>

            <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes}/>

          </Switch>
        </div>
      </Router>
  );
}

export default App;
