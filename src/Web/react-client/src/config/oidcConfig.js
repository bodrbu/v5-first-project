﻿import { ApplicationPaths } from "../features/auth/ApiAuthorizationConstants";

export const oidcConfig = {
  authority: "http://localhost:5003",
  client_id: "js",
  redirect_uri: `http://localhost:3000${ApplicationPaths.LoginCallback}`,
  response_type: "id_token token",
  scope: "openid profile learning performance",
  post_logout_redirect_uri: `http://localhost:3000${ApplicationPaths.LogOutCallback}`,
};
