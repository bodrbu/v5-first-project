import React, { useEffect } from 'react';
import { Button, Container, Jumbotron } from 'reactstrap';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { getCourseInfo, publishCourse, selectSingleCourseStatus } from "./coursesSlice";
import { getLessonsByCourse } from "../lessons/lessonsSlice";
import { getTasksByCourse } from "../tasks/tasksSlice";
import TasksList from "../tasks/TasksList";
import LessonsList from "../lessons/LessonsList";
import PerformanceInfo from "../users/PerformanceInfo";
import { getUserPerformance } from "../users/userSlice";

export default function CourseInfo() {

  debugger
  const dispatch = useDispatch()
  const {id} = useParams()
  const courseStatus = useSelector(selectSingleCourseStatus)

  const course = useSelector((state) => state.courses.selectedCourse)

  const lessons = useSelector((state) => state.lessons.lessons)
  const lessonsStatus = useSelector((state) => state.lessons.status)

  const tasks = useSelector((state) => state.tasks.tasks)
  const tasksStatus = useSelector((state) => state.tasks.status)

  const user = useSelector((state) => state.user.user)

  useEffect(() => {
    const isDifferentCourse = (course.id !== Number(id))
    const needDataLoading = (courseStatus === 'idle' || lessonsStatus === 'idle' || tasksStatus === 'idle' || isDifferentCourse)
    debugger
    if (needDataLoading) {
      dispatch(getCourseInfo(id))
      dispatch(getLessonsByCourse(id))
      dispatch(getTasksByCourse(id))
      dispatch(getUserPerformance(id))
    }
  }, [courseStatus, dispatch, id, lessonsStatus, tasksStatus])

  function publish() {
    dispatch(publishCourse())
  }

  function publishButton() {
    if (course.isDraft === true)
      return <Button className="btn btn-success" onClick={publish}>Опубликовать курс</Button>
    else
      return null;
  }

  const renderEditCourseButton = () => {
    return (<Link to={"/CourseEdit/" + id} className="btn btn-primary">Редактировать курс</Link>)
  }

  const renderAddLessonButton = () => {
    return (
        <Link className="btn btn-success"
              to={`/LessonAdd/${id}`}>
          Добавить урок
        </Link>
    )
  }

  const renderAddTaskButton = () => {
    return (
        <Link className="btn btn-success"
              to={`/TaskAdd/${id}`}>
          Добавить задачу
        </Link>
    )
  }

  function renderEditableComponent(func){
    if (course.userId === user.id) {
      return func()
    } else {
      return null
    }
  }

  const renderPerformanceInfo = () => {
    if (!!user && user.performance) {
      return (
          <>
            <PerformanceInfo taskCount={user.performance.taskCount} solvedTaskCount={user.performance.solvedTaskCount}/>
            <hr/>
          </>
      )
    } else {
      return null
    }
  }

  return (
      <Container className="App">

        <Jumbotron className="vertical-center">
          <h6>Организация: {course.organizationOwner.name} - {course.organizationOwner.description}</h6>
          <h1 className="display-4">Курс{course.isDraft ? "(ЧЕРНОВИК)" : ""} "{course.name}" </h1>
          <p>{course.description}</p>
        </Jumbotron>

        <Container>
          <hr/>
          {renderPerformanceInfo()}
          {renderEditableComponent(renderEditCourseButton)}
          {publishButton()}
          <hr/>
        </Container>

        <div className="row">
          <div className="col">
            <h3>Уроки курса:</h3>
            <hr/>
            {renderEditableComponent(renderAddLessonButton)}
            <hr/>
            <LessonsList canEdit={course.userId === user.id} lessons={lessons}/>
          </div>
          <div className="col">
            <h3>Задачи курса:</h3>
            <hr/>
            {renderEditableComponent(renderAddTaskButton)}
            <hr/>
            <TasksList canEdit={course.userId === user.id} tasks={tasks}/>
          </div>
        </div>
        <hr/>

      </Container>
  );
}