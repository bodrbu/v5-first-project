﻿import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import CoursesService from "./CoursesService";

const initialState = {
  courses: [],
  selectedCourse: {
    course: {},
    lessons: [],
    tasks: [],
    organizationOwner: '',
    status: 'idle'
  },
  status: 'idle',
  error: null
}

export const fetchCourses = createAsyncThunk(
    'courses/getAll',
    async () => {
      const response = await CoursesService.getAll()
      return response.data
    }
)

export const getCourseInfo = createAsyncThunk(
    'courses/getCourseInfo',
    async (courseId) => {
      const response = await CoursesService.get(courseId)
      return response.data
    }
)

export const addCourse = createAsyncThunk(
    'courses/add',
    async (data) => {
      await CoursesService.create(data)
    }
)

export const publishCourse = createAsyncThunk(
    'courses/publish',
    async (id) => {
      const response = await CoursesService.publish(id)
    }
)

export const updateCourse = createAsyncThunk(
    'courses/update',
    async (changes) => {
      const response = await CoursesService.update(changes)
      return changes
    }
)

const coursesSlice = createSlice({
  name: 'courses',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchCourses.pending]: (state, action) => {
      state.status = 'loading'
    },
    [fetchCourses.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.courses = state.courses.concat(action.payload)
    },
    [fetchCourses.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },

    [getCourseInfo.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getCourseInfo.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.selectedCourse = action.payload
    },
    [getCourseInfo.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },

    [addCourse.fulfilled]: (state, action) => {
      state.status = 'idle'
    },
    [addCourse.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },

    [updateCourse.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.selectedCourse = {...state.selectedCourse, ...action.payload}
      const course = state.courses.find(value => value.id === action.payload.id)
      course.name = action.payload.name
      course.description = action.payload.description
    },
    [updateCourse.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },

    [publishCourse.pending]: (state, action) => {
      state.status = 'loading'
    },
    [publishCourse.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.selectedCourse.isDraft = false
    },
    [publishCourse.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },
  }
})

export default coursesSlice.reducer

export const selectAllCourses = (state) => {
  return state.courses.courses
}

export const selectSingleCourseStatus = (state) => {
  return state.courses.selectedCourse.status
}