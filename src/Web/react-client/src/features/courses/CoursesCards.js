import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import { Card, CardHeader, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap';
import CourseOrg from "./CourseOrg";

const CoursesCards = (props) => {

  // useEffect(() => {
  //   console.log(props)
  //   debugger
  // },[])

  return (
      <div>
        <Card>
          <CardHeader>КУРС {props.obj.id} {props.obj.isDraft ? " (ЧЕРНОВИК)" : ""}</CardHeader>
          <CardBody>
            <CardTitle tag="h4">{props.obj.name}</CardTitle>

            <CardSubtitle tag="h6" className="mb-2 text-muted">
              <CourseOrg orgId={props.obj.organizationOwnerId}/>
            </CardSubtitle>
            <CardText>{props.obj.description}</CardText>
            <Link to={"/CourseInfo/" + props.obj.id} className="btn btn-success">Подробнее</Link>
          </CardBody>
        </Card>
        <p></p>
      </div>
  );
}

export default CoursesCards;