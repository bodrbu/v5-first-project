import React, { useEffect, useState } from 'react';
import { Form, FormGroup, Label, Input, Container, Jumbotron, Button } from 'reactstrap';
import CoursesService from "./CoursesService";
import UsersService from "../users/UsersService";
import { useDispatch } from "react-redux";
import { addCourse } from "./coursesSlice";
import { unwrapResult } from "@reduxjs/toolkit";
import { useHistory } from "react-router-dom";


const CourseAdd = () => {

  const dispatch = useDispatch()
  let history = useHistory()

  const initialState = {
    name: '',
    description: '',
    user: '',
    userID: 1
  }
  const [state, setState] = useState(initialState)

  const [addCourseStatus, setAddCourseStatus] = useState('idle')

  function getUserId() {
    ;//тут получаем ID пользователя, пока что оно 1
  }

  useEffect(() => {
    getUserId()
    console.log('userID', state.userID);
    getUser(state.userID)
  })

  function getUser() {
    console.log('state', state);
    UsersService.get(state.userID)
        .then(
            response => {
              setState({
                ...state,
                user: response.data
              });
            }
        )
        .catch(
            function (error) {
              console.log(error);
            }
        )
  }

  const canAdd = addCourseStatus === 'idle'

  const onSubmitClicked = async () => {
    if (!canAdd)
      return
    try {
      setAddCourseStatus('pending')
      const res = await dispatch(addCourse({
        name: state.name,
        description: state.description,
        organizationId: state.user.organizationId,
        userId: state.userID
      }))
      unwrapResult(res)
      setState(initialState)
      history.push('/CourseList');
    } catch (e) {
      console.error('Failed to add the course: ', e)
    } finally {
      setAddCourseStatus('idle')
    }
  }

  const handleChange = (e) => {
    setState({[e.target.name]: e.target.value});
  }

  return (

      <Container className="App">

        <Jumbotron className="vertical-center">
          <h1 className="display-4">Создание нового курса</h1>
        </Jumbotron>

        <Form>
          <FormGroup>
            <Label for="name">Наименование курса</Label>
            <Input type="text" name="name" id="name" placeholder="Введите наименование курса"
                   value={state.name} onChange={handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="description">Описание курса</Label>
            <Input type="text" name="description" id="description" placeholder="Введите описание курса"
                   value={state.description} onChange={handleChange}/>
          </FormGroup>
          <Button color="success" onClick={onSubmitClicked}>Создать черновик курса</Button>
          <Button color="danger">Отмена</Button>{' '}
        </Form>

      </Container>
  );
}

export default CourseAdd;