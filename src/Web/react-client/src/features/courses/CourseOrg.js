﻿import { useSelector } from "react-redux";
import { selectOrganization } from "../organizations/organizationsSlice";


const CourseOrg = ({orgId}) => {

  const organization = useSelector(state => selectOrganization(state, orgId))

  return (
      <span> Организация: {organization ? organization.name : 'Unknown author'}</span>
  )

}

export default CourseOrg