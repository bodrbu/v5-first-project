import React, { useEffect } from 'react';
import { Form, FormGroup, Label, Input, Container, Jumbotron, Button } from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { getCourseInfo, selectSingleCourseStatus, updateCourse } from "./coursesSlice";
import { useHistory, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";


const CourseEdit = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const {id} = useParams();

  const courseStatus = useSelector(selectSingleCourseStatus)
  const course = useSelector((state) => state.courses.selectedCourse)

  const {handleSubmit, register} = useForm()

  useEffect(() => {
    if (courseStatus === 'idle') {
      dispatch(getCourseInfo(id))
    }

  }, [courseStatus, dispatch])

  async function saveChangesAsync(data) {
    dispatch(updateCourse({
      id: Number(id),
      ...data
    }))
    history.push(`/CourseInfo/${id}`)
  }

  return (

      <Container className="App">
        <Jumbotron className="vertical-center">
          <h1 className="display-4">Редактирование курса</h1>
        </Jumbotron>
        <Form onSubmit={handleSubmit(saveChangesAsync)}>
          <FormGroup>
            <Label for="name">Наименование курса</Label>
            <Input type="text" name="name" id="name"
                   placeholder="Введите наименование курса"
                   innerRef={register({required: true})}
                   defaultValue={course.name}

            />
          </FormGroup>

          <FormGroup>
            <Label for="description">Описание курса</Label>
            <Input type="text" name="description" id="description" placeholder="Введите описание курса"
                   innerRef={register({required: true})}
                   defaultValue={course.description}/>
          </FormGroup>

          <Button color="success" className="m-1" type="submit">Обновить курс</Button>
          <Button color="danger" className="m-1" onClick={() => history.goBack()}>Отмена</Button>{' '}
        </Form>
      </Container>
  );
}

export default CourseEdit;