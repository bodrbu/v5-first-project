import React, { useEffect } from 'react';
import CoursesCards from './CoursesCards';
import { Container, Jumbotron } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { fetchCourses, selectAllCourses } from "./coursesSlice";
import { getAllOrganizations } from "../organizations/organizationsSlice";

export default function CoursesList() {
    const dispatch = useDispatch()

    const courses = useSelector(selectAllCourses)
    const courseStatus = useSelector((state) => state.courses.status)
    const organizationsStatus = useSelector((state) => state.organizations.status)
    const error = useSelector((state) => state.courses.error)

    useEffect(() => {
        if (courseStatus === 'idle') {
            dispatch(fetchCourses())
        }
        if (organizationsStatus === 'idle') {
            dispatch(getAllOrganizations())
        }
    }, [courseStatus, dispatch])

    function showCourses() {
        debugger
        return courses.map(function (res, i) {
            return <CoursesCards obj={res} key={i}/>
        });
    }

    return (
        <Container className="App">

            <Jumbotron className="vertical-center">
                <h1 className="display-4">Список курсов</h1>
            </Jumbotron>

            <Container>
                <hr/>
                <Link to={"/CourseAdd"} className="btn btn-primary">Добавить курс</Link>
                <hr/>
            </Container>

            {showCourses()}

        </Container>
    );
}