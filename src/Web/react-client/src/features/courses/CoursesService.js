import { createAuthClient } from "../../api/http-common";

//Получить все курсы
const getAll = () => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get("/Courses");
};

//Получить выбранный курс
const get = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Courses/${id}`);
};

//Создать черновик курса
const create = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post("/Courses", data);
};

//Опубликовать данные курса
const publish = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put(`/Courses/${id}/publish`);
};

//Обновить данные курса
const update = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put("/Courses", data);
};

//Удалить курс
const remove = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).delete(`/Courses/${id}`);
};

//Получить список курсов выбранного пользователя (создателя курсов)
const getOwnerAll = (userId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Courses/withowner/${userId}`);
};

//Получить список курсов выбранного пользователя
const getUserAll = (userId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Courses/users/${userId}`);
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  getOwnerAll,
  getUserAll,
  publish
};