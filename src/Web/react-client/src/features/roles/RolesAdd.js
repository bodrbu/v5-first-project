import React from 'react';
import { Form,  FormGroup, Label, Input, Container, Jumbotron, Button } from 'reactstrap';
import RolesService from "./RolesService";



class RolesAdd extends React.Component{

    constructor(props){
        super(props);
        this.state = {

            name: '',
            description: '',

        }
    }



    AddRoles = () => {
        RolesService.create(
        {
            id: 0,
            name: this.state.name,
            description: this.state.description,
        })
        .then(json => {
            if(json.status === 201) {
                console.log(json.data.Status);
                alert('Data saved successfully');
                this.props.history.push('/RolesList');
            }
            else
            {
                alert('Data not saved');
                debugger;
                this.props.history.push('/RolesList');
            }
        })
    }


    handleChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
        return(

            <Container className="App">

                <Jumbotron className="vertical-center">
                    <h1 className="display-4">Создание роли</h1>
                </Jumbotron>


                <Form>

                    <FormGroup>
                        <Label for="name">Наименование роли</Label>
                        <Input type="text" name="name" id="name" placeholder="Введите наименование роли" value = {this.state.name} onChange={this.handleChange}/>
                    </FormGroup>

                    <FormGroup>
                        <Label for="description">Описание роли</Label>
                        <Input type="text" name="description" id="description" placeholder="Введите описание роли" value = {this.state.description} onChange={this.handleChange}/>
                    </FormGroup>

                    <Button color="success" onClick={this.AddRoles}>Создать роль</Button>
                    <Button color="danger">Отмена</Button>{' '}

                </Form>

            </Container>


        );
    }



}

export default RolesAdd;