import { createAuthClient } from "../../api/http-common";

//Получить все роли
const getAll = () => {
  return createAuthClient().get("/Roles");
};

//Получить выбранную роль
const get = id => {
  return createAuthClient().get(`/Roles/${id}`);
};

//Создать роль
const create = data => {
  return createAuthClient().post("/Roles", data);
};

//Обновить роль
const update = data => {
  return createAuthClient().put("/Roles", data);
};

//Удалить роль
const remove = id => {
  return createAuthClient().delete(`/Roles/${id}`);
};



export default {
  getAll,
  get,
  create,
  update,
  remove
};