import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import RolesService from "./RolesService";


class RolesTabRow extends Component {

    DeleteRole = () => {
        RolesService.remove(this.props.obj.id)
        .then(json => {
            if (json.status === 204) {
                alert('Роль успешно удалёна');
                window.location.reload();
            }
        })
    }

    render() {
        return(
            <tr>
                <td>{ this.props.obj.name}</td>
                <td>{ this.props.obj.description}</td>
                <td>
                    <Link to = {"/RolesEdit/" + this.props.obj.id} className="btn btn-success">Редактировать</Link>
                    <button type="button" onClick={this.DeleteRole} className="btn btn-danger">Удалить</button>
                </td>
            </tr>
        );
    }

}
export default RolesTabRow;
