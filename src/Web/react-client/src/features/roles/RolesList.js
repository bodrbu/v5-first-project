import React from 'react';
import RolesService from "./RolesService";
import RolesTabRow from './RolesTabRow';
import { Container, Jumbotron, Form } from 'reactstrap';
import { Link } from 'react-router-dom';

export default class RolesList extends React.Component {

    constructor(props) {
        super(props)
        this.state = { roles: [] }

    }

    componentDidMount() {
        this.getCourses();
    }
    componentDidUpdate(prevProps) {
        console.log("this.props.location.pathname", this.props.location.pathname);
        console.log("prevProps.location.pathname", prevProps.location.pathname);


        if (this.props.location.pathname !== prevProps.location.pathname) {
            this.getCourses();
        }

    }

    getCourses() {
        debugger;
        RolesService.getAll()
            .then(
                response => {
                    this.setState({ roles: response.data }, () => {
                        console.log(this.state.roles, 'Roles');
                    });
                }
            )
            .catch(
                function (error) {
                    console.log(error);
                }
            )
    }


    tabRow() {
        return this.state.roles.map(function (res, i) {
            return <RolesTabRow obj={res} key={i} />
        });
    }

    render() {

        return (
            <Container className="App">

                <Jumbotron className="vertical-center">
                    <h1 className="display-4">Список ролей</h1>
                </Jumbotron>

                <Container>
                    <hr/>
                    <Link to={"/RolesAdd"} className="btn btn-primary">Добавить роль</Link>
                    <hr />
                </Container>


                <Form className="form">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Описание</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.tabRow()}
                        </tbody>

                    </table>

                </Form>
            </Container>
        );
    };

}