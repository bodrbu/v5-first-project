﻿import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import UsersService from "./UsersService";


const initialState = {
  user: {},
  performance: {},
  status: 'idle',
  error: null
}

export const getUserByIdentity = createAsyncThunk(
    'user/getByIdentity',
    async (userIdentity) => {
      const response = await UsersService.getByIdentity(userIdentity)
      return response.data
    }
)

export const getUserPerformance = createAsyncThunk(
    'user/performance',
    async (courseId, { getState }) => {
      console.log(getState())
      const userId = getState().user.user.id
      const response = await UsersService.getCoursePerformance(userId, courseId)
      return response.data
    }
)

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    taskSolved(state, action) {
      state.user.performance.solvedTaskCount++
    },
  },
  extraReducers: {
    [getUserByIdentity.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getUserByIdentity.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.user = {...state.user, ...action.payload}
    },
    [getUserByIdentity.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },
    [getUserPerformance.fulfilled]: (state, action) => {
      state.user.performance = action.payload
    },
    [getUserPerformance.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },
  }
})

export const { taskSolved } = userSlice.actions

export const selectUserPerformanceByCourse = (state) => {
  return state.user.performance
}

export default userSlice.reducer