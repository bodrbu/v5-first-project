import { createAuthClient } from "../../api/http-common";

//Получить всех пользователей
const getAll = () => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get("/Users");
};

//Получить данные пользователя по id
const get = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Users/${id}`);
};

//Получить данные пользователя по identity
const getByIdentity = identityGuid => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Users/by-identity/${identityGuid}`);
};

const getCoursePerformance = (userId, courseId) => {
  return createAuthClient(process.env.REACT_APP_PERFORMANCE_API_URL).get(`v1/performance/summary/user/${userId}/course/${courseId}`);
}

//Создать пользователя
const create = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post("/Users", data);
};

//Обновить пользователя
const update = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put("/Users", data);
};

//Удалить пользователя
const remove = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).delete(`/Users/${id}`);
};


export default {
  getAll,
  get,
  getByIdentity,
  getCoursePerformance,
  create,
  update,
  remove
};