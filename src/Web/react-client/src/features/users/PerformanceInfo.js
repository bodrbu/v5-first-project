﻿import React from 'react';

export default function PerformanceInfo({taskCount, solvedTaskCount}) {
  return (
      <p>Вы решили <b>{solvedTaskCount}</b> из <b>{taskCount}</b> задач</p>
  )
}