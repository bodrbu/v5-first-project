import React, { useEffect, useState } from 'react';
import { Form, FormGroup, Label, Input, Container, Jumbotron, Button } from 'reactstrap';
import OrganizationService from "./OrganizationsService";
import UsersService from "../users/UsersService";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";


const OrganizationAdd = () => {
  let history = useHistory()
  const dispatch = useDispatch()

  const [state, setState] = useState({
    name: '',
    description: ''
  })

  const addOrganization = () => {
    dispatch(addOrganization({
      name: state.name,
      description: state.description,
    }))
    history.push('/OrganizationList');
  }

  const handleChange = (e) => {
    setState({[e.target.name]: e.target.value});
  }

  return (

      <Container className="App">

        <Jumbotron className="vertical-center">
          <h1 className="display-4">Создание организации</h1>
        </Jumbotron>

        <Form>
          <FormGroup>
            <Label for="name">Наименование организации</Label>
            <Input type="text" name="name" id="name" placeholder="Введите наименование организации"
                   value={state.name} onChange={handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="description">Описание организации</Label>
            <Input type="text" name="description" id="description" placeholder="Введите описание организации"
                   value={state.description} onChange={handleChange}/>
          </FormGroup>
          <Button color="success" onClick={addOrganization}>Создать организацию</Button>
          <Button color="danger" onClick={history.goBack}>Отмена</Button>
        </Form>
      </Container>
  );

}

export default OrganizationAdd;