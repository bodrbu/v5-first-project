﻿import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import OrganizationsService from "./OrganizationsService";
import deleteArrayElement from "../../helpers/deleteArrayElement";

const initialState = {
  organizations: [],
  status: 'idle',
  error: null
}

export const getAllOrganizations = createAsyncThunk(
    'organizations/all',
    async () => {
      const response = await OrganizationsService.getAll()
      return response.data
    }
)

export const getOrganization = createAsyncThunk(
    'organizations/getById',
    async (orgId) => {
      const response = await OrganizationsService.get(orgId)
      return response.data
    }
)

export const addOrganization = createAsyncThunk(
    'organizations/add',
    async (data) => {
      await OrganizationsService.create(data)
    }
)

export const updateOrganization = createAsyncThunk(
    'organizations/update',
    async (data) => {
      await OrganizationsService.update(data)
    }
)

export const removeOrganization = createAsyncThunk(
    'organizations/remove',
    async (orgId) => {
      await OrganizationsService.remove(orgId)
    }
)

const organizationsSlice = createSlice({
  name: 'organizations',
  initialState,
  reducers: {},
  extraReducers: {
    [getAllOrganizations.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getAllOrganizations.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.organizations = state.organizations.concat(action.payload)
    },
    [getAllOrganizations.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    },

    [addOrganization.fulfilled]: (state, action) => {
      state.organizations.push(action.payload)
    },
    [addOrganization.rejected]: (state, action) => {
      state.error = action.error.message
    },

    [updateOrganization.fulfilled]: (state, action) => {
      const {id, name, description} = action.payload
      const organization = selectOrganization(state, id)
      if (organization) {
        organization.name = name
        organization.description = description
      }
    },
    [updateOrganization.rejected]: (state, action) => {
      state.error = action.error.message
    },

    [removeOrganization.fulfilled]: (state, action) => {
      const id = action.payload
      const org = selectOrganization(state, id)
      deleteArrayElement(state.organizations, org)
    },
  }
})

export const selectOrganization = (state, id) => {
  return state.organizations.organizations.find((org) => org.id === Number(id))
}

export default organizationsSlice.reducer