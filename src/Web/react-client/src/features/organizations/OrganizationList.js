import React, { useEffect } from 'react';
import OrganizationTabRow from './OrganizationTabRow';
import { Container, Jumbotron, Form } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { getAllOrganizations } from "./organizationsSlice";


export default function OrganizationList() {
  const dispatch = useDispatch()

  const organizations = useSelector((state) => state.organizations.organizations)
  const organizationStatus = useSelector((state) => state.organizations.status)
  const error = useSelector((state) => state.organizations.error)

  useEffect(() => {
    if (organizationStatus === 'idle') {
      dispatch(getAllOrganizations())
    }

  }, [organizationStatus, dispatch])

  const tabRow = () => {
    return organizations.map(org => {
      return <OrganizationTabRow org={org} key={org.id}/>
    });
  }

  return (
      <Container className="App">

        <Jumbotron className="vertical-center">
          <h1 className="display-4">Список организаций</h1>
        </Jumbotron>

        <Container>
          <hr/>
          <Link to={"/OrganizationAdd"} className="btn btn-primary">Добавить организаций</Link>
          <hr/>
        </Container>


        <Form className="form">
          <table className="table table-striped">
            <thead>
            <tr>
              <th>Наименование</th>
              <th>Описание</th>
              <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            {tabRow()}
            </tbody>

          </table>
        </Form>


      </Container>
  );

}