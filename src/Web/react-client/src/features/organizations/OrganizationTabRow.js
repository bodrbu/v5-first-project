import React from 'react';
import { Link } from 'react-router-dom'
import { useDispatch } from "react-redux";
import { removeOrganization } from "./organizationsSlice";


const OrganizationTabRow = ({org}) => {

  const dispatch = useDispatch()

  const deleteOrganization = () => {
    dispatch(removeOrganization(org.id))
  }

  return (
      <tr>
        <td>{org.name}</td>
        <td>{org.description}</td>
        <td>
          <Link to={"/OrganizationEdit/" + org.id} className="btn btn-success">Редактировать</Link>
          <button type="button" onClick={deleteOrganization} className="btn btn-danger">Удалить</button>
        </td>
      </tr>
  );
}
export default OrganizationTabRow;
