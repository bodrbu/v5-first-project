import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Container, Jumbotron, Button } from 'reactstrap';
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { selectOrganization, updateOrganization } from "./organizationsSlice";


const OrganizationEdit = () => {

  const dispatch = useDispatch()
  const {id} = useParams()
  let history = useHistory()

  const organization = useSelector((state) => selectOrganization(state, id))

  const [state, setState] = useState({
    name: organization.name,
    description: organization.description
  })

  const editOrganization = () => {
    dispatch(updateOrganization({
      id: Number(id),
      name: state.name,
      description: state.description,
    }))
    history.push('/OrganizationsList');
  }

  const handleChange = (e) => {
    setState({[e.target.name]: e.target.value});
  }

  return (
      <Container className="App">

        <Jumbotron className="vertical-center">
          <h1 className="display-4">Редактирование организации</h1>
        </Jumbotron>

        <Form>
          <FormGroup>
            <Label for="name">Наименование организации</Label>
            <Input type="text" name="name" id="name" placeholder="Введите наименование организации"
                   value={state.name} onChange={handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="description">Описание организации</Label>
            <Input type="text" name="description" id="description" placeholder="Введите описание организации"
                   value={state.description} onChange={handleChange}/>
          </FormGroup>
          <Button color="success" onClick={editOrganization}>Обновить организацию</Button>
          <Button color="danger">Отмена</Button>{' '}
        </Form>

      </Container>
  );
}

export default OrganizationEdit;