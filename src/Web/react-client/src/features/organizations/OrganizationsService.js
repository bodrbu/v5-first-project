import { createAuthClient } from "../../api/http-common";

//Получить все организации
const getAll = () => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get("/Organizations");
};

//Получить выбранную организацию
const get = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Organizations/${id}`);
};

//Создать организацию
const create = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post("/Organizations", data);
};

//Обновить данные организации
const update = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put("/Organizations", data);
};

//Удалить организацию
const remove = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).delete(`/Organizations/${id}`);
};

//Получить организацию по курсу
const getWithCourse = (courseId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Organizations/withcourse/${courseId}`);
};

//Получить список курсов выбранной организации
const getCourses = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Organizations/${id}/courses`);
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  getWithCourse,
  getCourses
};