﻿import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Button, Container, Form, FormFeedback, FormGroup, Input, Label } from 'reactstrap';
import TasksService from "./TasksService";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { getTaskById, selectSingleTask } from "./tasksSlice";
import { taskSolved } from "../users/userSlice";


export default function TaskSolve() {
  const dispatch = useDispatch()
  let history = useHistory()
  const [apiResponse, setApiResponse] = useState(null);
  let {id} = useParams();
  const {register, handleSubmit, errors} = useForm();
  const task = useSelector(selectSingleTask)

  const user = useSelector((state) => state.user.user)

  useEffect(() => {
    if (task.status === 'idle') {
      dispatch(getTaskById(id))
    }
  }, [task.status, dispatch, id]);

  async function handleClickAsync(data) {
    try {
      await TasksService.trySolve({
        userId: user.id,
        courseId: task.courseId,
        taskId: task.id,
        answer: data.answer
      });
      setApiResponse(true);
      dispatch(taskSolved())
    } catch (e) {
      console.log(e.response);
      setApiResponse(false);
    }
  }

  let info;
  if (apiResponse === null) {
    info = null;
  } else if (apiResponse === true) {
    info = <h4 className="text-success"> Правильно!</h4>;
  } else {
    info = <h4 className="text-info"> Попробуйте другой вариант.</h4>
  }

  return (
      <Container className="App">
        <div>
          <h3>Описание задачи</h3>
          <p>{task.description}</p>
        </div>

        {info}

        <Form onSubmit={handleSubmit(handleClickAsync)}>
          <FormGroup>
            <Label for="answer">Ваш ответ:</Label>
            <Input type="text" id="answer" name="answer"
                   innerRef={register({required: true})}
                   invalid={errors.answer}
            />
            {errors.answer && <FormFeedback>Напишите ответ</FormFeedback>}
          </FormGroup>
            <Button className="m-1" color="primary" type="submit">
              Отправить ответ
            </Button>
            <Button className="m-1" color="secondary" onClick={history.goBack}>
              Назад
            </Button>
        </Form>
      </Container>
  )
}