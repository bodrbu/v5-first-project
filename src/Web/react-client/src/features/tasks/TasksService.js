import { createAuthClient } from "../../api/http-common";
import { Tasks as API } from "./TasksApi"

//Получить домашнее задание по id
const get = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(API.getById(id));
};

//Добавить задачу к курсу
const create = (request) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post(API.create(),request);
};

//Обновить задачу
const update = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put(API.update(), data);
};

//Удалить задачу
const remove = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).delete(API.remove(id));
};

//Получить список задач выбранного курса
const getWithCourse = (courseId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(API.getWithCourseId(courseId));
};

const trySolve = (data) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post(API.trySolveTask(), data)
}


export default {
  get,
  create,
  update,
  remove,
  getWithCourse,
  trySolve
};