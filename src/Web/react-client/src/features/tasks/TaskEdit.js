﻿import React, { useEffect } from 'react';
import { useForm } from "react-hook-form";
import { useHistory, useParams } from 'react-router-dom';
import {
  Container,
  Jumbotron,
  FormGroup, Form, Label, Input, Button, FormFeedback
} from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { getTaskById, selectSingleTask, updateTask } from "./tasksSlice";


export default function TaskEdit() {
  const dispatch = useDispatch()
  const {register, handleSubmit, errors} = useForm();
  let history = useHistory();
  let {id} = useParams();

  const task = useSelector(selectSingleTask)

  useEffect(() => {
    if (task.status === 'idle') {
      dispatch(getTaskById(id))
    }
  }, [task.status, dispatch, id]);

  async function saveChangesAsync(data) {
    dispatch(updateTask({
      id: Number(id),
      ...data,
      courseId: task.courseId
    }))
    history.push(`/CourseInfo/${task.courseId}`)
  }

  return (
      <Container className="App">
        <Jumbotron className="vertical-center">
          <h1 className="display-6">Редактирование задачи</h1>
        </Jumbotron>
        <Form onSubmit={handleSubmit(saveChangesAsync)} className="text-left">
          <FormGroup>
            <Label for="name">Название задачи</Label>
            <Input type="text" name="name" id="name"
                   innerRef={register({required: true})}
                   invalid={errors.name}
                   defaultValue={task.name}
            />
            {errors.name && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label for="description">Условие задачи</Label>
            <Input type="text" name="description" id="description"
                   innerRef={register({required: true})}
                   invalid={errors.description}
                   defaultValue={task.description}
            />
            {errors.description && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label for="answer">Правильный ответ на задачу</Label>
            <Input type="text" name="answer" id="answer"
                   innerRef={register({required: true})}
                   invalid={errors.answer}
                   defaultValue={task.answer}
            />
            {errors.answer && <FormFeedback>Напишите ответ</FormFeedback>}
          </FormGroup>
          <FormGroup className="text-center">
            <Button color="primary" className="m-1" type="submit">
              Сохранить
            </Button>
            <Button className="m-1" onClick={() => history.goBack()}>
              Отменить
            </Button>
          </FormGroup>
        </Form>
      </Container>
  )
}