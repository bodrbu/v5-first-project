import React from 'react';
import { Link } from 'react-router-dom'
import { Card, CardBody, CardTitle, CardText, Button } from 'reactstrap';

export default function TaskItem(props) {

  const renderEditAndRemoveButtons = () => {
    if (props.canEdit) {
      return (
          <>
            <Link to={"/TaskEdit/" + props.obj.id} className="btn btn-info m-1">Редактировать</Link>
            <Button color="danger" onClick={() => props.deleteCallback()}>Удалить задачу</Button>
          </>
      )
    } else {
      return null
    }
  }

  return (
      <div>
        <Card>
          <CardBody>
            <CardTitle tag="h4">{props.obj.name}</CardTitle>
            <CardText>{props.obj.description}</CardText>
            <Link to={"/TaskSolve/" + props.obj.id}
                  className="btn btn-warning m-1">Подробнее</Link>
            {renderEditAndRemoveButtons()}
          </CardBody>
        </Card>
        <p></p>
      </div>
  );
}