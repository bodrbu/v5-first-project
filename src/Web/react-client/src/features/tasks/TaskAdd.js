﻿import React, { useState } from "react";
import { Button, Container, Form, FormFeedback, FormGroup, Input, Jumbotron, Label } from "reactstrap";
import { useHistory, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { createTask } from "./tasksSlice";


export default function TaskAdd() {
  const dispatch = useDispatch()
  const {courseId} = useParams();
  let history = useHistory();
  const {register, handleSubmit, errors} = useForm();

  const [addRequestStatus, setAddRequestStatus] = useState('idle')

  async function submitForm(data) {
    if (addRequestStatus !== 'idle')
      return
    try {
      setAddRequestStatus('pending')
      const values = {...data, courseId: Number(courseId)}
      const resultAction = dispatch(createTask(values))
      history.push(`/CourseInfo/${courseId}`)
      unwrapResult(resultAction)
    } catch (err) {
      alert("Что-то пошло не так...")
      history.goBack();
      console.error(err.response);
    } finally {
      setAddRequestStatus('idle')
    }
  }

  return (
      <Container className="App">
        <Jumbotron className="vertical-center">
          <h1 className="display-6">Создание новой задачи</h1>
        </Jumbotron>
        <Form onSubmit={handleSubmit(submitForm)} className="text-left">
          <FormGroup>
            <Label for="name">Название задачи</Label>
            <Input type="text" name="name" id="name"
                   innerRef={register({required: true})}
                   invalid={errors.name}
            />
            {errors.name && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label for="description">Описание задачи</Label>
            <Input type="text" name="description" id="description"
                   innerRef={register({required: true})}
                   invalid={errors.description}
            />
            {errors.description && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label for="answer">Ответ на задачу</Label>
            <Input type="text" name="answer" id="answer"
                   innerRef={register({required: true})}
                   invalid={errors.answer}
            />
            {errors.answer && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup className="text-center">
            <Button color="primary" type="submit">Создать</Button>
          </FormGroup>
        </Form>
      </Container>
  )
}