﻿import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import TasksService from "./TasksService";
import deleteArrayElement from "../../helpers/deleteArrayElement";


const initialTaskState = {
  id: 0,
  name: '',
  description: '',
  answer: '',
  courseId: 0,
  status: 'idle'
}

const initialState = {
  tasks:[],
  selectedTask: initialTaskState,
  status: 'idle',
  error: null
}


export const getTasksByCourse = createAsyncThunk(
    'tasks/getByCourse',
    async (courseId) => {
      const response = await TasksService.getWithCourse(courseId)
      return response.data
    }
)

export const getTaskById = createAsyncThunk(
    'tasks/getById',
    async (taskId) => {
      debugger
      const response = await TasksService.get(taskId)
      return response.data
    }
)

export const createTask = createAsyncThunk(
    'tasks/create',
    async (data) => {
      await TasksService.create(data)
    }
)

export const updateTask = createAsyncThunk(
    'tasks/update',
    async (data) => {
      await TasksService.update(data)
    }
)

export const deleteTask = createAsyncThunk(
    'tasks/delete',
    async (taskId) => {
      await TasksService.remove(taskId)
    }
)

export const solveTask = createAsyncThunk(
    'tasks/solve',
    async (data) => {
      await TasksService.trySolve(data)
    }
)


const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {},
  extraReducers: {
    [getTasksByCourse.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.tasks = action.payload
    },
    [getTaskById.fulfilled]: (state, action) => {
      debugger
      state.selectedTask.status = 'succeeded'
      state.selectedTask = action.payload
    },
    [createTask.fulfilled]: (state, action) => {
      state.status = 'idle'
    },
    [updateTask.fulfilled]: (state, action) => {
      state.selectedTask = {...state.selectedTask, ...action.payload}
    },
    [deleteTask.fulfilled]: (state, action) => {
      const id = Number(action.payload)
      const task = state.tasks.find(t => t.id === id)
      deleteArrayElement(state.tasks, task)
      if (state.selectedTask.id === id) {
        state.selectedTask = initialTaskState
      }
    },
    [deleteTask.rejected]: (state, action) => {
      console.log(action.payload)
    }
  }
})


export const selectSingleTask = (state) => {
  return state.tasks.selectedTask
}


export default tasksSlice.reducer