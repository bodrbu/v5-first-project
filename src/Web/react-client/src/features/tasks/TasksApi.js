﻿
const TaskName = "Tasks"

export class Tasks{

    static getById(id){
        return `${TaskName}/${id}`
    }

    static getWithCourseId(courseId){
        return `${TaskName}/course/${courseId}`
    }

    static create(){
        return `${TaskName}/`
    }

    static update(){
        return `${TaskName}/`
    }

    static remove(id){
        return `${TaskName}/${id}`
    }

  static trySolveTask() {
        return `${TaskName}/try-solve/`;
  }
}
