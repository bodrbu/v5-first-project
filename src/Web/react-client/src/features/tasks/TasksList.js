﻿import React from 'react';
import TaskItem from "./TaskItem";
import { deleteTask } from "./tasksSlice";
import { useDispatch } from "react-redux";


const TasksList = ({canEdit, tasks}) => {
  const dispatch = useDispatch()

  async function removeTask(taskId) {
    const yes = window.confirm("Вы уверены, что хотите удалить задачу?")
    if (yes) {
      dispatch(deleteTask(taskId))
    }
  }

  return (
      tasks.map(function (res) {
        return <TaskItem obj={res} key={res.id}
                         canEdit={canEdit}
                         deleteCallback={async () => await removeTask(res.id)}/>
      })
  )
}

export default TasksList