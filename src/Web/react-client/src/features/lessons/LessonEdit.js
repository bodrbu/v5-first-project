﻿import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory, useParams } from "react-router-dom";
import {
  Container,
  Jumbotron,
  FormGroup, Form, Label, Input, Button, FormFeedback
} from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { getLessonById, selectSingleLesson, updateLesson } from "./lessonsSlice";


export default function LessonEdit() {
  const dispatch = useDispatch()
  const {register, handleSubmit, errors} = useForm()
  let history = useHistory()
  let {id} = useParams()

  const lesson = useSelector(selectSingleLesson)

  useEffect(() => {
    if (lesson.status === 'idle') {
      dispatch(getLessonById(id))
    }
  }, [lesson.status, dispatch, id]);

  async function saveChangesAsync(data) {
    dispatch(updateLesson({
      id: Number(id),
      ...data
    }))
    history.push(`/CourseInfo/${lesson.courseId}`)
  }

  return (
      <Container className="App">
        <Jumbotron className="vertical-center">
          <h1 className="display-6">Редактирование урока</h1>
        </Jumbotron>
        <Form onSubmit={handleSubmit(saveChangesAsync)} className="text-left">
          <FormGroup>
            <Label for="name">Название урока</Label>
            <Input type="text" name="name" id="name"
                   innerRef={register({required: true})}
                   invalid={errors.name}
                   defaultValue={lesson.name}
            />
            {errors.name && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>
          <FormGroup>
            <Label for="description">Описание урока</Label>
            <Input type="text" name="description" id="description"
                   innerRef={register({required: true})}
                   invalid={errors.description}
                   defaultValue={lesson.description}
            />
            {errors.description && <FormFeedback>Данное поле обязательно</FormFeedback>}
          </FormGroup>

          <FormGroup>
            <Label for="videoUri">Ссылка на видео</Label>
            <Input type="text" name="videoUri" id="videoUri"
                   innerRef={register({required: true})}
                   invalid={errors.videoUri}
                   defaultValue={lesson.videoUri}
            />
            {errors.videoUri && <FormFeedback>Ссылка на видео не может быть пустой</FormFeedback>}
          </FormGroup>
          <FormGroup className="text-center">
            <Button color="primary" className="m-1" type="submit">
              Сохранить
            </Button>
            <Button className="m-1" onClick={() => history.goBack()}>
              Отменить
            </Button>
          </FormGroup>
        </Form>
      </Container>
  );
}