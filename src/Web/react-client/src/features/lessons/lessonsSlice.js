﻿import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import LessonsService from "./LessonsService";
import deleteArrayElement from "../../helpers/deleteArrayElement";

const initialSelectedLesson = {
  id: 0,
  name: '',
  description: '',
  videoUri: '',
  courseId: 0,
  status: 'idle'
}

const initialState = {
  lessons: [],
  selectedLesson: initialSelectedLesson,
  status: 'idle',
  error: null
}

export const getLessonsByCourse = createAsyncThunk(
    'lessons/getByCourse',
    async (courseId) => {
      const response = await LessonsService.getWithCourse(courseId)
      return response.data
    }
)

export const getLessonById = createAsyncThunk(
    'lessons/getById',
    async (lessonId) => {
      const response = await LessonsService.get(lessonId)
      return response.data
    }
)

export const createLesson = createAsyncThunk(
    'lessons/create',
    async (data) => {
      await LessonsService.create(data)
    }
)

export const updateLesson = createAsyncThunk(
    'lessons/update',
    async (data) => {
      await LessonsService.update(data)
    }
)

export const deleteLesson = createAsyncThunk(
    'lessons/delete',
    async (lessonId) => {
      await LessonsService.remove(lessonId)
    }
)


const lessonsSlice = createSlice({
  name: 'lessons',
  initialState,
  reducers: {},
  extraReducers: {
    [getLessonsByCourse.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.lessons = action.payload
    },
    [getLessonById.fulfilled]: (state, action) => {
      state.selectedLesson.status = 'succeeded'
      state.selectedLesson = action.payload
    },
    [createLesson.fulfilled]: (state, action) => {

      state.status = 'idle'
    },
    [updateLesson.fulfilled]: (state, action) => {
      state.selectedLesson = {...state.selectedLesson, ...action.payload}
    },
    [deleteLesson.fulfilled]: (state, action) => {
      const id = Number(action.payload)
      const lesson = state.lessons.find(l => l.id === id)
      deleteArrayElement(state.lessons, lesson)
      if (state.selectedLesson.id === id) {
        state.selectedLesson = initialSelectedLesson
      }
    },
    [deleteLesson.rejected]: (state, action) => {
      console.log(action.payload)
    }
  }
})


export const selectSingleLesson = (state) => {
  return state.lessons.selectedLesson
}


export default lessonsSlice.reducer