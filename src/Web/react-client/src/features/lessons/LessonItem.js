import React from 'react';
import { Link } from 'react-router-dom'
import { Card, CardBody, CardTitle, CardText, Button } from 'reactstrap';

export default function LessonItem(props) {

  const renderEditableParts = () => {
    if (props.canEdit) {
      return (
          <>
            <Link to={"/editLesson/" + props.obj.id} className="btn btn-info m-1">Редактировать</Link>
            <Button color="danger" onClick={() => props.deleteCallback()}>Удалить урок</Button>
          </>
      )
    } else {
      return null
    }
  }

  return (
      <div>
        <Card>
          <CardBody>
            <CardTitle tag="h4">{props.obj.name}</CardTitle>
            <CardText>{props.obj.description}</CardText>
            <Link to={"/LessonDetails/" + props.obj.id} className="btn btn-secondary m-1">Подробнее</Link>
            {renderEditableParts()}
          </CardBody>
        </Card>
        <p></p>
      </div>

  );
}