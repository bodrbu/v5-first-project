import { createAuthClient } from "../../api/http-common";

//Получить выбранный урок
const get = id => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Lessons/${id}`);
};

//Добавить занятие (урок) к курсу
const create = (data) => {
  const url = '/Lessons/';
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).post(url, data);
};

//Изменить занятие
const update = data => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).put("/Lessons", data);
};

//Удалить урок
const remove = (lessonId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).delete(`/Lessons/${lessonId}`);
}

//Получить все уроки выбранного курса
const getWithCourse = (courseId) => {
  return createAuthClient(process.env.REACT_APP_LEARNING_API_URL).get(`/Lessons/course/${courseId}`);
};



export default {
  get,
  create,
  update,
  remove,
  getWithCourse
};