import React, { useEffect } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import {
  Container, Button
} from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { getLessonById, selectSingleLesson } from "./lessonsSlice";


export default function LessonDetails() {
  const dispatch = useDispatch()
  const lesson = useSelector(selectSingleLesson)
  const {id} = useParams()
  let history = useHistory()

  useEffect(() => {
    dispatch(getLessonById(id))
  }, [dispatch, id]);


  return (
      <Container>
        <br/>
        <div className="text-center"><h3>{lesson.name}</h3></div>
        <br/>
        <br/>
        <div>
          <p>{lesson.description}</p>
        </div>
        <div>
          <Link to={lesson.videoUri}>Ссылка на видео</Link>
        </div>
        <div className="text-center">
          <Button className=" m-1" onClick={() => history.goBack()}>
            К странице курса
          </Button>
        </div>
      </Container>
  )

}