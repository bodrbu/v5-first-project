﻿import React from 'react';
import LessonItem from "./LessonItem";
import { deleteLesson } from "./lessonsSlice";
import { useDispatch } from "react-redux";


const LessonsList = ({canEdit, lessons}) => {
  const dispatch = useDispatch()

  async function removeLesson(lessonId) {
    const yes = window.confirm("Вы уверены, что хотите удалить урок?")
    if (yes) {
      dispatch(deleteLesson(lessonId))
    }
  }

  return (
      lessons.map(function (res) {
        return <LessonItem obj={res} key={res.id}
                           canEdit={canEdit}
                           deleteCallback={async () => await removeLesson(res.id)}/>
      })
  )
}

export default LessonsList