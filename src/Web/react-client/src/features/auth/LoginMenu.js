import React, { Fragment, useEffect, useState } from 'react';
import { NavbarText, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import authService from './AuthorizeService';
import { ApplicationPaths } from './ApiAuthorizationConstants';
import { useDispatch } from "react-redux";
import { getUserByIdentity } from "../users/userSlice";


export const LoginMenu = () => {

  const initialState = {
    isAuthenticated: false,
    userName: null
  }
  const [state, setState] = useState(initialState)

  const dispatch = useDispatch()

  useEffect(async () => {
    const populateState = async () => {
      const [isAuthenticated, user] = await Promise.all([authService.isAuthenticated(), authService.getUser()])
      if (isAuthenticated) {
        dispatch(getUserByIdentity(user.sub))
      }
      setState({
        isAuthenticated,
        userName: user && user.name
      })
    }
    const subscription = authService.subscribe(populateState)
    await populateState()
    return () => {
      authService.unsubscribe(subscription)
    }
  }, [])


  const authenticatedView = (userName, profilePath, logoutPath) => {
    return (<Fragment>
      <NavbarText>
        <NavLink tag={Link} className="text-dark" to={profilePath}>Привет, {userName}!</NavLink>
      </NavbarText>
      <NavbarText>
        <NavLink tag={Link} className="text-dark" to={logoutPath}>Logout</NavLink>
      </NavbarText>
    </Fragment>)

  }

  const anonymousView = (registerPath, loginPath) => {
    return (<Fragment>
      <NavbarText>
        <NavLink tag={Link} className="text-dark" to={registerPath}>Register</NavLink>
      </NavbarText>
      <NavbarText>
        <NavLink tag={Link} className="text-dark" to={loginPath}>Login</NavLink>
      </NavbarText>
    </Fragment>);
  }

  if (!state.isAuthenticated) {
    const registerPath = `${ApplicationPaths.Register}`;
    const loginPath = `${ApplicationPaths.Login}`;
    return anonymousView(registerPath, loginPath);
  } else {
    const profilePath = `${ApplicationPaths.Profile}`;
    const logoutPath = {pathname: `${ApplicationPaths.LogOut}`, state: {local: true}};
    return authenticatedView(state.userName, profilePath, logoutPath);
  }
}
