import axios from "axios";

import authService from "../features/auth/AuthorizeService";

export function createAuthClient(baseURL) {
  const client = axios.create({
    baseURL: baseURL,
    headers: {
      "Content-type": "application/json"
    }
  })

  client.interceptors.request.use(async function (config) {
    const token = await authService.getAccessToken();
    if (!!token)
      config.headers.Authorization = `Bearer ${token}`;
    return config;
  })

  return client
}

export function createClient() {
  return axios.create({
    baseURL: process.env.REACT_APP_LEARNING_API_URL,
    headers: {
      "Content-type": "application/json"
    }
  })
}