﻿/**
 * Удаляет элемент в массиве
 * @param array массив
 * @param elem элемент для удаления
 */
const deleteArrayElement = (array, elem) => {
  const idx = array.indexOf(elem)
  array.splice(idx, 1)
}

export default deleteArrayElement