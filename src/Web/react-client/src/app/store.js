﻿import { configureStore } from "@reduxjs/toolkit";

import coursesReducer from "../features/courses/coursesSlice"
import organizationsReducer from "../features/organizations/organizationsSlice";
import lessonsReducer from "../features/lessons/lessonsSlice";
import tasksReducer from "../features/tasks/tasksSlice";
import userReducer from "../features/users/userSlice";

export default configureStore({
    reducer: {
        courses: coursesReducer,
        organizations: organizationsReducer,
        lessons: lessonsReducer,
        tasks: tasksReducer,
        user: userReducer
    }
})