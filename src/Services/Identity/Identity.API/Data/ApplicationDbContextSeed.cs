﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Identity.API.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Identity.API.Data
{
    public class ApplicationDbContextSeed
    {
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher = new PasswordHasher<ApplicationUser>();

        public async Task SeedAsync(ApplicationDbContext context, IWebHostEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, int retry = 0)
        {
            int retryForAvaiability = retry;

            try
            {
                if (!context.Users.Any())
                {
                    await context.Users.AddRangeAsync(GetDefaultUsers());
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex, "EXCEPTION ERROR while migrating {DbContextName}",
                        nameof(ApplicationDbContext));

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }

        private IEnumerable<ApplicationUser> GetDefaultUsers()
        {
            var leninUser = new ApplicationUser
            {
                Country = "Russia",
                Email = "lenin@example.com",
                Id = "95018970-e218-4589-b187-cd937defd210",
                LastName = "Ленин",
                Name = "Владимир",
                PhoneNumber = "1234567890",
                UserName = "lenin@example.com",
                NormalizedEmail = "LENIN@EXAMPLE.COM",
                NormalizedUserName = "LENIN@EXAMPLE.COM",
                SecurityStamp = "e621d16d-2528-47c4-88b0-84dad233feec",
                Birthdate = new DateTime(1870, 4, 22)
            };

            leninUser.PasswordHash = _passwordHasher.HashPassword(leninUser, "Pass@word1");

            var stalinUser = new ApplicationUser
            {
                Country = "Russia",
                Email = "stalin@example.com",
                Id = "bd257d1e-5e72-43ca-9c99-2634d746d399",
                LastName = "Сталин",
                Name = "Иосиф",
                PhoneNumber = "1234567890",
                UserName = "stalin@example.com",
                NormalizedEmail = "STALIN@EXAMPLE.COM",
                NormalizedUserName = "STALIN@EXAMPLE.COM",
                SecurityStamp = "3d685769-d59d-4e54-821c-7c23a8fd1dd8",
                Birthdate = new DateTime(1878, 12, 18)
            };

            stalinUser.PasswordHash = _passwordHasher.HashPassword(stalinUser, "Pass@word1");

            return new List<ApplicationUser>
            {
                leninUser,
                stalinUser
            };
        }
    }
}