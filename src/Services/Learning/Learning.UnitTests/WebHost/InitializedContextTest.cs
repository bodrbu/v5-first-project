﻿using System;
using System.Data.Common;
using DataAccess.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace EduForm.UnitTests.WebHost
{
    /// <summary>
    /// Шаблон для тестирования сервисов.
    /// Заполняет базу Sqlite в памяти стандартным набором объектов
    /// </summary>
    public abstract class InitializedContextTest : DbContextTestBase<AppDbcontext>, IDisposable
    {
        private readonly DbConnection _connection;

        public InitializedContextTest()
            : base(
                new DbContextOptionsBuilder<AppDbcontext>()
                    .UseSqlite(CreateInMemoryDatabase())
                    .EnableSensitiveDataLogging()
                    .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;
            SeedCoursesDb();
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        private void SeedCoursesDb()
        {
            using var ctx = new AppDbcontext(ContextOptions);

            ctx.Database.EnsureDeleted();
            ctx.Database.EnsureCreated();

            new EfDbInitializer(ctx).Initialize();
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}