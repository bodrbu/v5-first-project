﻿using System;
using Microsoft.EntityFrameworkCore;

namespace EduForm.UnitTests.WebHost
{
    public abstract class DbContextTestBase<TContext> where TContext : DbContext, IDisposable
    {
        protected readonly DbContextOptions<TContext> ContextOptions;

        protected DbContextTestBase(DbContextOptions<TContext> contextOptions)
        {
            ContextOptions = contextOptions;
        }
    }
}