﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using FluentAssertions;
using Learning.API.Models.Courses;
using Learning.API.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EduForm.UnitTests.WebHost.Services
{
    public class OrganizationServiceTests : InitializedContextTest
    {
        [Fact]
        public async Task GetAllAsync_ReturnsEntities()
        {
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(context);

            var entities = await sut.GetAllAsync();

            entities.Count().Should().Be(FakeDataFactory.FOrganizations.Count);
        }

        [Fact]
        public async Task GetByIdAsync_OrgNotExist_ThorwsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            const int notExistingId = 1111;
            Func<Task> act = async () => await sut.GetByIdAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task GetByIdAsync_ValidOrgId_ReturnsEntity()
        {
            const int orgId = 1;
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            var entity = await sut.GetByIdAsync(orgId);

            entity.Should().NotBeNull();
        }

        [Fact]
        public async Task GetByCourseIdAsync_ValidCourseId_ReturnsEntity()
        {
            const int courseId = 1;
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            var entity = await sut.GetByCourseIdAsync(courseId);

            entity.Should().NotBeNull();
        }

        [Fact]
        public async Task GetByCourseIdAsync_CourseNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            const int notExistingId = 1111;
            Func<Task> act = async () => await sut.GetByCourseIdAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task GetOrganizationCoursesAsync_ValidCourseId_ReturnsEntity()
        {
            const int orgId = 1;
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            var entity = await sut.GetOrganizationCoursesAsync(orgId);

            entity.Count().Should().Be(1);
        }

        [Fact]
        public async Task GetOrganizationCoursesAsync_CourseNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(ctx);

            const int notExistingId = 1111;
            Func<Task> act = async () => await sut.GetOrganizationCoursesAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task AddAsync_RoleExist_AddEntity()
        {
            const string expectedName = "Test Name";
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var request = new OrganizationResponse()
                {
                    Name = expectedName,
                    Description = "Test Description"
                };

                var sut = new OrganizationService(ctx);

                await sut.AddAsync(request);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Organizations.ToListAsync();
            res.Count().Should().Be(FakeDataFactory.FOrganizations.Count + 1);
        }

        [Fact]
        public async Task UpdateAsync_RoleExist_UpdateEntity()
        {
            const int orgId = 1;
            var expectedName = "Test Name";
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var request = new OrganizationResponse()
                {
                    Id = orgId,
                    Name = expectedName,
                    Description = "Test Description"
                };

                var sut = new OrganizationService(ctx);

                await sut.UpdateAsync(request);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Organizations.SingleOrDefaultAsync(org => org.Id == orgId);
            res.Name.Should().BeEquivalentTo(expectedName);
        }

        [Fact]
        public async Task UpdateAsync_RoleNotExist_ThrowsException()
        {
            const int orgId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(context);

            Func<Task> t = async () => await sut.UpdateAsync(new OrganizationResponse() {Id = orgId});

            t.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task RemoveAsync_OrgExist_RemoveEntity()
        {
            const int orgId = 1;
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var sut = new OrganizationService(ctx);

                await sut.RemoveAsync(orgId);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Organizations.ToListAsync();
            res.Count().Should().Be(FakeDataFactory.FOrganizations.Count - 1);
        }

        [Fact]
        public async Task RemoveAsync_OrgNotExist_ThrowsException()
        {
            const int orgId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new OrganizationService(context);

            Func<Task> t = async () => await sut.RemoveAsync(orgId);

            t.Should().Throw<EntityNotFoundDomainException>();
        }
    }
}