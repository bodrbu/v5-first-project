﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Courses;
using Core.Domain.Exceptions;
using DataAccess.Data;
using FluentAssertions;
using Learning.API.Models.Courses;
using Learning.API.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EduForm.UnitTests.WebHost.Services
{
    // SQLite can also use databases created purely in-memory. This is easy to use with EF Learning.Domain as long
    // as you understand the in-memory database lifetime:
    //    The database is created when the connection to it is opened
    //    The database is deleted when the connection to it is closed
    public sealed class CourseServiceTests : InitializedContextTest
    {
        [Fact]
        public async Task GetAllAsync_ReturnNotNull()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new CourseService(ctx);

            var res = await sut.GetAllAsync();

            res.Count().Should().Be(2);
        }

        [Fact]
        public async Task GetByIdAsync_EntityExist_ReturnsEntity()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int existingId = 1;
            var sut = new CourseService(ctx);

            var res = await sut.GetByIdAsync(existingId);

            res.Should().NotBeNull();
        }

        [Fact]
        public async Task GetByIdAsync_EntityNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int notExistingId = 1111;
            var sut = new CourseService(ctx);

            Func<Task> act = async () => await sut.GetByIdAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>()
                .WithMessage($"Курс с Id = {notExistingId} не найден");
        }

        [Fact]
        public async Task GetByOwnerUserIdAsync_UserDoesNotHasCourses_ReturnsEmptyList()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int notExistingId = 1111;
            var sut = new CourseService(ctx);

            var res = await sut.GetByOwnerUserIdAsync(notExistingId);

            res.Should().BeEmpty();
        }

        [Fact]
        public async Task GetWithUserIdAsync_UserNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int notExistingId = 1111;
            var sut = new CourseService(ctx);

            Func<Task> act = async () => await sut.GetWithUserIdAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task CreateDraftAsync_CorrectInput_ReturnsNewId()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new CourseService(ctx);
            var request = new CourseDraftRequest()
            {
                Name = "Test Name",
                Description = "Test Description",
                OrganizationId = 1,
                UserId = 1
            };

            var id = await sut.CreateDraftAsync(request);

            id.Should().Be(3);
        }

        [Fact]
        public async Task PublishCourseAsync_CourseNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int notExistingId = 1111;
            var sut = new CourseService(ctx);

            Func<Task> act = async () => await sut.PublishCourseAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task PublishCourseAsync_CourseIsNotDraft_ThrowsException()
        {
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var course = new Course()
                {
                    Name = "Test Name",
                    Description = "Test Description",
                    OrganizationOwnerId = 1,
                    UserOwnerId = 1,
                    IsDraft = false
                };
                await ctx.AddAsync(course);
                await ctx.SaveChangesAsync();

                int existingId = course.Id;
                var sut = new CourseService(ctx);

                Func<Task> act = async () => await sut.PublishCourseAsync(existingId);
                act.Should().Throw<EduFormException>().WithMessage("Курс уже опубликован");
            }
        }

        [Fact]
        public async Task UpdateAsync_CourseNotFound_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var request = new UpdateCourseRequest()
            {
                Id = 1111,
                Name = "Test Name",
                Description = "Test Description",
                OrganizationId = 1,
                UserId = 1
            };

            var sut = new CourseService(ctx);

            Func<Task> act = async () => await sut.UpdateAsync(request);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task UpdateAsync_CorrectData_UpdateEntity()
        {
            var courseId = 1;
            var expectedName = "Test Name";
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var request = new UpdateCourseRequest()
                {
                    Id = courseId,
                    Name = expectedName,
                    Description = "Test Description",
                    OrganizationId = 1,
                    UserId = 2
                };

                var sut = new CourseService(ctx);

                await sut.UpdateAsync(request);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Courses.SingleOrDefaultAsync(course => course.Id == courseId);
            res.Name.Should().BeEquivalentTo(expectedName);
        }

        [Fact]
        public async Task DeleteCourseAsync_CourseNotExist_ThrowsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            const int notExistingId = 1111;
            var sut = new CourseService(ctx);

            Func<Task> act = async () => await sut.DeleteAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }
    }
}