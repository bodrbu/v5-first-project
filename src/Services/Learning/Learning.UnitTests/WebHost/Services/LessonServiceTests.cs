﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using FluentAssertions;
using Learning.API.Services;
using Xunit;

namespace EduForm.UnitTests.WebHost.Services
{
    public class LessonServiceTests : InitializedContextTest
    {
        [Fact]
        public async Task GetCourseLessonsAsync_ValidCourseId_ReturnsLessons()
        {
            var courseId = 1;
            var expectedCount = 2;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new LessonService(context);

            var lessons = await sut.GetCourseLessonsAsync(courseId);

            lessons.Count().Should().Be(expectedCount);
        }

        [Fact]
        public async Task GetCourseLessonsAsync_NotExistingCourseId_ThrowsException()
        {
            var courseId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new LessonService(context);

            Func<Task> t = async () => await sut.GetCourseLessonsAsync(courseId);

            t.Should().Throw<EntityNotFoundDomainException>();
        }
        
        [Fact]
        public async Task GetByIdAsync_ValidId_ReturnsEntity()
        {
            var lessonId = 1;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new LessonService(context);

            var lesson = await sut.GetByIdAsync(lessonId);

            lesson.Should().NotBeNull();
        }

        [Fact]
        public async Task GetByIdAsync_NotExistingId_ThrowsException()
        {
            var lessonId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new LessonService(context);

            Func<Task> t = async () => await sut.GetByIdAsync(lessonId);

            t.Should().Throw<EntityNotFoundDomainException>();
        }
    }
}