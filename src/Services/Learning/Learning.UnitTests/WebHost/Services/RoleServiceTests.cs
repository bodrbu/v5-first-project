﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using FluentAssertions;
using Learning.API.Models.Users;
using Learning.API.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EduForm.UnitTests.WebHost.Services
{
    public class RoleServiceTests : InitializedContextTest
    {
        [Fact]
        public async Task GetAllAsync_ReturnsNotNull()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new RoleService(ctx);

            var roles = await sut.GetAllAsync();
            var roleDtos = roles.ToList();

            roleDtos.Should().NotBeEmpty();
            roleDtos.Count.Should().Be(2);
        }

        [Fact]
        public async Task GetByIdAsync_RoleNotExist_ThorwsException()
        {
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new RoleService(ctx);

            int notExistingId = 1111;
            Func<Task> act = async () => await sut.GetByIdAsync(notExistingId);

            act.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task GetByIdAsync_ValidRoleId_ReturnsEntity()
        {
            var roleId = 1;
            await using var ctx = new AppDbcontext(ContextOptions);
            var sut = new RoleService(ctx);

            var role = await sut.GetByIdAsync(roleId);

            role.Should().NotBeNull();
        }

        [Fact]
        public async Task AddAsync_RoleExist_AddEntity()
        {
            const string expectedName = "Test Name";
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var request = new RoleDto()
                {
                    Name = expectedName,
                    Description = "Test Description"
                };

                var sut = new RoleService(ctx);

                await sut.AddAsync(request);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Roles.ToListAsync();
            res.Count().Should().Be(FakeDataFactory.FRoles.Count + 1);
        }

        [Fact]
        public async Task UpdateAsync_RoleExist_UpdateEntity()
        {
            var rId = 1;
            var expectedName = "Test Name";
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var request = new RoleDto()
                {
                    Id = rId,
                    Name = expectedName,
                    Description = "Test Description"
                };

                var sut = new RoleService(ctx);

                await sut.UpdateAsync(request);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Roles.SingleOrDefaultAsync(role => role.Id == rId);
            res.Name.Should().BeEquivalentTo(expectedName);
        }

        [Fact]
        public async Task UpdateAsync_RoleNotExist_ThrowsException()
        {
            const int roleId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new RoleService(context);

            Func<Task> t = async () => await sut.UpdateAsync(new RoleDto {Id = roleId});

            t.Should().Throw<EntityNotFoundDomainException>();
        }

        [Fact]
        public async Task RemoveAsync_RoleExist_RemoveEntity()
        {
            var rId = 1;
            await using (var ctx = new AppDbcontext(ContextOptions))
            {
                var sut = new RoleService(ctx);

                await sut.RemoveAsync(rId);
            }

            await using var ctx2 = new AppDbcontext(ContextOptions);
            var res = await ctx2.Roles.ToListAsync();
            res.Count().Should().Be(FakeDataFactory.FRoles.Count - 1);
        }

        [Fact]
        public async Task RemoveAsync_RoleNotExist_ThrowsException()
        {
            const int roleId = 1111;
            await using var context = new AppDbcontext(ContextOptions);
            var sut = new RoleService(context);

            Func<Task> t = async () => await sut.RemoveAsync(roleId);

            t.Should().Throw<EntityNotFoundDomainException>();
        }
    }
}