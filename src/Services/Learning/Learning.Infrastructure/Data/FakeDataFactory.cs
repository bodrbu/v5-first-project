﻿using System;
using System.Collections.Generic;
using Core.Domain.Courses;
using Core.Domain.PersonalInformation;


namespace DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static readonly Guid FirstUserGuid = Guid.Parse("95018970-e218-4589-b187-cd937defd210");
        private static readonly Guid SecondUserGuid = Guid.Parse("bd257d1e-5e72-43ca-9c99-2634d746d399");

        ///<summary>Инициализирующий список пользователей</summary>
        public static List<User> FUsers => new List<User>()
        {
            new User()
            {
                Name = "Владимир",
                Surname = "Ленин",
                Login = "Lenin",
                Password = "111",
                RoleId = 2,
                OrganizationId = 1,
                IdentityGuid = FirstUserGuid
            },
            new User()
            {
                Name = "Иосиф",
                Surname = "Сталин",
                Login = "Stalin",
                Password = "222",
                RoleId = 1,
                OrganizationId = 2,
                IdentityGuid = SecondUserGuid
            },
        };

        ///<summary>Инициализирующий список ролей</summary>
        public static List<Role> FRoles => new List<Role>()
        {
            new Role()
            {
                Name = "Pupil",
                Description = "Ученик. Проходит курсы и всё такое",
            },
            new Role()
            {
                Name = "Teacher",
                Description = "Преподаватель. Создаёт курсы и всё такое"
            }
        };

        ///<summary>Инициализирующий список курсов</summary>
        public static List<UserCourse> FUserHasCourses => new List<UserCourse>()
        {
            new UserCourse()
            {
                UserId = 1,
                CourseId = 1,
            },

            new UserCourse()
            {
                UserId = 1,
                CourseId = 2,
            }
        };

        ///<summary>Инициализирующий список курсов</summary>
        public static List<Course> FCourses => new List<Course>()
        {
            new Course()
            {
                Name = "Политика 20",
                Description = "Политика России 20го века",
                OrganizationOwnerId = 1,
                UserOwnerId = 1,
            },

            new Course()
            {
                Name = "Философия 19",
                Description = "Филосовские размышления 19го века",
                OrganizationOwnerId = 2,
                UserOwnerId = 2,
            }
        };

        public static List<Lesson> FLessons => new List<Lesson>
        {
            new Lesson
            {
                Title = "Первый урок",
                Description = "Первое описание",
                VideoUri = "example.com/1",
                CourseId = 1
            },
            new Lesson
            {
                Title = "Второй урок",
                Description = "Второе описание",
                VideoUri = "example.com/2",
                CourseId = 1
            },
            new Lesson
            {
                Title = "Третий урок",
                Description = "Третье описание",
                VideoUri = "example.com/3",
                CourseId = 2
            },
            new Lesson
            {
                Title = "Четвёртый урок",
                Description = "Четвёртое описание",
                VideoUri = "example.com/4",
                CourseId = 2
            },
            new Lesson
            {
                Title = "Пятый урок",
                Description = "Пятое описание",
                VideoUri = "example.com/5",
                CourseId = 2
            },
        };

        ///<summary>Инициализирующий список задач</summary>
        public static List<HomeWorkTask> FTasksHWs => new List<HomeWorkTask>()
        {
            new HomeWorkTask()
            {
                Name = "Задача №1",
                Description = "Открыть гугл и начать искать это, а потом записать в хронологическом порядке",
                CourseId = 1,
                Answer = "Ответ на задачу №1"
            },

            new HomeWorkTask()
            {
                Name = "Задача №2",
                Description = "Переписать полученный список в обратном порядке",
                CourseId = 1,
                Answer = "Ответ на задачу №2"
            },

            new HomeWorkTask()
            {
                Name = "Задача №3",
                Description = "Открыть гугл и начать искать это, а потом забыть что искал",
                CourseId = 2,
                Answer = "Ответ на задачу №3"
            },

            new HomeWorkTask()
            {
                Name = "Задача №4",
                Description = "Вспомнить что искал и снова забыть",
                CourseId = 2,
                Answer = "Ответ на задачу №4"
            },
        };

        ///<summary>Инициализирующий список организаций</summary>
        public static List<Organization> FOrganizations => new List<Organization>()
        {
            new Organization()
            {
                Name = "ВКПБ",
                Description = "Всесоюзная коммунистическая партия",
            },
            new Organization()
            {
                Name = "РСФСР",
                Description = "Российская Советская Федеративная Социалистическая Республика",
            }
        };
    }
}