﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Core.Domain;
using Core.Domain.Courses;
using Core.Domain.PersonalInformation;
using DataAccess.TypeConfigurations;
using Microsoft.EntityFrameworkCore;


namespace DataAccess.Data
{
    public class AppDbcontext : DbContext
    {
        public DbSet<User> Users => Set<User>();
        public DbSet<Role> Roles => Set<Role>();
        public DbSet<Course> Courses => Set<Course>();
        public DbSet<Lesson> Lessons => Set<Lesson>();
        public DbSet<Comment> Comments => Set<Comment>();
        public DbSet<HomeWorkTask> TaskHWs => Set<HomeWorkTask>();
        public DbSet<UserCourse> UserCourse => Set<UserCourse>();
        public DbSet<Organization> Organizations => Set<Organization>();

        public AppDbcontext(DbContextOptions<AppDbcontext> dbContextOptions) : base(dbContextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var entries = ChangeTracker.Entries<BaseEntity>()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified);

            foreach (var entry in entries)
            {
                entry.Entity.DateModified = DateTime.Now;
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreate = DateTime.Now;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries<BaseEntity>()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified);

            foreach (var entry in entries)
            {
                entry.Entity.DateModified = DateTime.Now;
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreate = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}