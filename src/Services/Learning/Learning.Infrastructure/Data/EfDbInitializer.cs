﻿using Core.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Data
{
    public class EfDbInitializer : IInitialize
    {
        private readonly AppDbcontext _dataContext;

        public EfDbInitializer(AppDbcontext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.Migrate();

            _dataContext.AddRange(FakeDataFactory.FOrganizations);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FRoles);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FUsers);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FCourses);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FLessons);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FTasksHWs);
            _dataContext.SaveChanges();
            _dataContext.AddRange(FakeDataFactory.FUserHasCourses);
            _dataContext.SaveChanges();
        }
    }
}