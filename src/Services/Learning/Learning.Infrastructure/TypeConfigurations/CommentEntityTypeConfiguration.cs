﻿using Core.Domain.Courses;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class CommentEntityTypeConfiguration:IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(c=>c.Lesson)
                .WithMany(l => l.Comments)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
