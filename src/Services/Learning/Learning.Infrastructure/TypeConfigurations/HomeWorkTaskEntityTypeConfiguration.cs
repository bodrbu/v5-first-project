﻿using Core.Domain.Courses;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class HomeWorkTaskEntityTypeConfiguration:IEntityTypeConfiguration<HomeWorkTask>
    {
        public void Configure(EntityTypeBuilder<HomeWorkTask> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(50);
            builder.Property(x => x.Description).HasMaxLength(200);
            builder.HasOne(x => x.Course)
                .WithMany(work => work.Tasks);
        }
    }
}
