﻿using Core.Domain.Courses;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class UserCourseEntityTypeConfiguration: IEntityTypeConfiguration<UserCourse>
    {
        public void Configure(EntityTypeBuilder<UserCourse> builder)
        {
            builder.HasKey(bc => new { bc.UserId, bc.CourseId });
            builder
                .HasOne(bc => bc.User)
                .WithMany(e => e.CourseUsers)
                .HasForeignKey(uc => uc.UserId);
            builder
                .HasOne(bc => bc.Course)
                .WithMany(e => e.CourseUsers)
                .HasForeignKey(uc => uc.CourseId);
        }
    }
}
