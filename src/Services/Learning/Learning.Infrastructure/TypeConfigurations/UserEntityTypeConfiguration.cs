﻿using Core.Domain.PersonalInformation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(50);
            builder.Property(x => x.Surname).HasMaxLength(50);
            builder.Property(x => x.Login).HasMaxLength(50);
            builder.Property(x => x.Password).HasMaxLength(50);
            builder.HasOne(x => x.Role).WithMany();
            builder.HasOne(p => p.Organization)
                .WithMany(o => o.Users)
                .OnDelete(DeleteBehavior.SetNull);
            builder.Property(x => x.OrganizationId).IsRequired(false);
        }
    }
}