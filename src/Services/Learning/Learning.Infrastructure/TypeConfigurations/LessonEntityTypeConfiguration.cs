﻿using Core.Domain.Courses;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class LessonEntityTypeConfiguration:IEntityTypeConfiguration<Lesson>
    {
        public void Configure(EntityTypeBuilder<Lesson> builder)
        {
            builder
                .HasOne(l=>l.Course)
                .WithMany(course => course.Lessons)
                .IsRequired();
        }
    }
}
