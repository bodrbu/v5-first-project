﻿using Core.Domain.Courses;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.TypeConfigurations
{
    class CourseEntityTypeConfiguration: IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(50);
            builder.Property(x => x.Description).HasMaxLength(200);
            builder.HasOne(x => x.OrganizationOwner).WithMany();
            builder.HasOne(x => x.UserOwner).WithMany();
            builder.HasMany(c => c.Tasks);
            builder.HasMany(c => c.Lessons)
                .WithOne(lesson => lesson.Course).IsRequired();
        }
    }
}
