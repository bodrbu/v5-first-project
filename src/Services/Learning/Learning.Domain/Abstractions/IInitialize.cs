﻿namespace Core.Abstractions
{
    public interface IInitialize
    {
        public void Initialize();
    }
}
