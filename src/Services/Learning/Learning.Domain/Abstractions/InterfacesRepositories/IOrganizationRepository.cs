﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Domain.Courses;

namespace Core.Abstractions.InterfacesRepositories
{
    /// <summary>
    /// Интерфейсы для взимодействия с хранилищем организация
    /// </summary>
    public interface IOrganizationRepository
    {
        /// <summary>
        /// Добавление новой организации
        /// </summary>
        /// <param name="newOrganization">Организация</param>
        /// <returns></returns>
        Task AddOrganization(Organization newOrganization);

        /// <summary>
        /// Получить информацию по организации
        /// </summary>
        /// <param name="idOrganization">ID организации</param>
        /// <returns></returns>
        Task<Organization> GetOrganization(int idOrganization);

        /// <summary>
        /// Получить список всех организций
        /// </summary>
        /// <returns></returns>
        Task<List<Organization>> GetAllOrganizations();

        /// <summary>
        /// Изменение информации об организации
        /// </summary>
        /// <param name="organization">Организация</param>
        /// <returns></returns>
        Task UpdateOrganization(Organization organization);

        /// <summary>
        /// Закрытие организации
        /// </summary>
        /// <param name="idOrganization">ID организации</param>
        /// <returns></returns>
        Task CloseOrganization(int idOrganizationForClose);
    }
}
