﻿using System.Threading.Tasks;
using Core.Domain.Courses;

namespace Core.Abstractions.InterfacesRepositories
{
    /// <summary>
    /// Интерфейс для взаимодействия с хранилищем курсов
    /// </summary>
    public interface ICourseRepository
    {
        /// <summary>
        /// Добавлние нового курса
        /// </summary>
        /// <param name="newCourse">Новый курс</param>
        /// <returns></returns>
        Task AddCourse(Course newCourse);

        /// <summary>
        /// Получить курс по его ID
        /// </summary>
        /// <param name="idCourse">ID курса</param>
        /// <returns></returns>
        Task<Course> GetCourseAsync(int idCourse);

        /// <summary>
        /// Изменение информации в курсе
        /// </summary>
        /// <param name="updatedCourse"></param>
        /// <returns></returns>
        Task UpdateAsync(Course updatedCourse);

        /// <summary>
        /// Удалить выбранный курс
        /// </summary>
        /// <param name="courseId">Идетнификатор курса</param>
        /// <returns></returns>
        public Task RemoveCourse(int courseId);
    }
}