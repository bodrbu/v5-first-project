﻿using System;
using System.Threading.Tasks;
using Core.Domain.PersonalInformation;

namespace Core.Abstractions.InterfacesRepositories
{
    /// <summary>
    /// Интерфейсы для взаимодействия с хранилищем пользователей
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Создать нового пользователя
        /// </summary>
        /// <param name="newUser">Новый пользователь</param>
        /// <returns></returns>
        Task AddUser(User newUser);

        /// <summary>
        /// Получить пользователя по его ID
        /// </summary>
        /// <param name="idUser">ID пользователя</param>
        /// <returns></returns>
        Task<User> GetUser(int idUser);

        /// <summary>
        /// Получить пользователя по его логину
        /// </summary>
        /// <param name="login">Логин</param>
        /// <returns></returns>
        Task<User> GetUser(string login);

        /// <summary>
        /// Получить пользователя по связке: ФИО + Др
        /// </summary>
        /// <param name="surname">Фамилия</param>
        /// <param name="name">Имя</param>
        /// <param name="patronimyc">Отчество</param>
        /// <param name="birthdate">Дата рождения</param>
        /// <returns></returns>
        Task<User> GetUser(string surname, string name, string patronimyc, DateTime birthdate);

        /// <summary>
        /// Обновить информацию пользователя
        /// </summary>
        /// <param name="userForUpdate">Обновляемый пользователь</param>
        /// <returns></returns>
        Task UpdateUser(User userForUpdate);

        /// <summary>
        /// Отключение записи пользователя
        /// </summary>
        /// <param name="idUser">ID пользователя для деактивации</param>
        /// <returns></returns>
        Task UserOff(int idUser);
    }
}
