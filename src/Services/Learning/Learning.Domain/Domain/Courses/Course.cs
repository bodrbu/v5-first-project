﻿using System;
using System.Collections.Generic;
using Core.Domain.PersonalInformation;

namespace Core.Domain.Courses
{
    /// <summary>
    /// Курсы
    /// </summary>
    public class Course
        : BaseEntity
    {
        ///<summary>
        /// Наименование курса
        /// </summary>
        public string Name { get; set; }

        ///<summary>
        /// Описание курса
        /// </summary>
        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        ///<summary>
        /// Организация, к которой относится курс
        /// </summary>
        public Organization OrganizationOwner { get; set; }
        public int OrganizationOwnerId { get; set; }
        
        ///<summary>
        /// Пользователь, который создал курс
        /// </summary>
        public User UserOwner { get; set; }
        public int UserOwnerId { get; set; }

        /// <summary>
        /// Список занятий данного курса
        /// </summary>
        public List<Lesson> Lessons { get; set; }

        ///<summary>
        /// Список пользователей имеющих данный курс
        /// </summary>
        public List<UserCourse> CourseUsers { get; set; } = new List<UserCourse>();

        ///<summary>
        /// Список домашних заданий данного курса
        /// </summary>
        public List<HomeWorkTask> Tasks { get; set; } = new List<HomeWorkTask>();

        /// <summary>
        /// Является ли курс черновиком
        /// </summary>
        public bool IsDraft { get; set; }
    }
}
