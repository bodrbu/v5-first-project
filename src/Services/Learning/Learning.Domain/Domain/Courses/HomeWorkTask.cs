﻿namespace Core.Domain.Courses
{
    /// <summary>
    /// Задача домашнего задания
    /// </summary>
    public class HomeWorkTask
        : BaseEntity
    {
        /// <summary>
        /// Курс, для которого создано домашнее задание
        /// </summary>
        public Course Course { get; set; }

        public int CourseId { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание задачи
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Правильный ответ на задачу
        /// </summary>
        public string Answer { get; set; }
    }
}