﻿using Core.Domain.PersonalInformation;

namespace Core.Domain.Courses
{
    public class UserCourse
    {
        public int CourseId { get; set; }
        public Course Course { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
