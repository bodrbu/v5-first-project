﻿using System.Collections.Generic;
using Core.Domain.PersonalInformation;

namespace Core.Domain.Courses
{
    /// <summary>
    /// Организация
    /// </summary>
    public class Organization
        : BaseEntity
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание организации
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Признак закрытия организации
        /// </summary>
        public bool IsClosed { get; set; }

        /// <summary>
        /// Список курсов организации
        /// </summary>
        public List<Course> Courses { get; set; }

        /// <summary>
        /// Список пользователей, принадлежащих к данной организации
        /// </summary>
        public List<User> Users { get; set; }
    }
}
