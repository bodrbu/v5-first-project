﻿using System;
using System.Collections.Generic;

namespace Core.Domain.Courses
{
    /// <summary>
    /// Урок
    /// </summary>
    public class Lesson
        : BaseEntity
    {
        /// <summary>
        ///  Наименование занятия
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание занятия
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на видео для курса
        /// </summary>
        public string VideoUri { get; set; }

        /// <summary>
        /// Курс к которому относится занятие
        /// </summary>
        public Course Course { get; set; }
        public int CourseId { get; set; }

        /// <summary>
        /// Комментарии к уроку
        /// </summary>
        public List<Comment> Comments { get; set; }

        /// <summary>
        /// Дата проведения занятия
        /// </summary>
        public DateTime DateLesson { get; set; }
    }
}