﻿using System;
using Core.Domain.PersonalInformation;

namespace Core.Domain.Courses
{
    public class Comment : BaseEntity
    {
        /// <summary>
        /// Пользователь, опубликовавший комментарий
        /// </summary>
        public User Publisher { get; set; }
        public int PublisherId { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Урок, к которому относится комментарий
        /// </summary>
        public Lesson Lesson { get; set; }
        public int LessonId { get; set; }
    }
}
