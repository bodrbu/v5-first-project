﻿using System;
#pragma warning disable 8632

namespace Core.Domain.Exceptions
{
    public class EduFormException: Exception
    {
        public EduFormException()
        {
            
        }

        public EduFormException(string? message): base(message)
        {
            
        }
        
        public EduFormException(string? message, Exception? innerException): base(message, innerException)
        {
            
        }
    }
}