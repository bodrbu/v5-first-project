﻿using System;
#pragma warning disable 8632

namespace Core.Domain.Exceptions
{
    public class EntityNotFoundDomainException : EduFormException
    {
        public EntityNotFoundDomainException()
        {
        }

        public EntityNotFoundDomainException(string? message) : base(message)
        {
        }

        public EntityNotFoundDomainException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}