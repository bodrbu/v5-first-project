﻿using System;

namespace Core.Domain
{

    public class BaseEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Дата модификации записи
        /// </summary>
        public DateTime DateModified { get; set; }
    }
}
