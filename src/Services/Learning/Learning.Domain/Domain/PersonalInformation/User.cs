﻿using System;
using System.Collections.Generic;
using Core.Domain.Courses;

namespace Core.Domain.PersonalInformation
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
        : BaseEntity
    {

        /// <summary>
        /// Guid из IdentityService
        /// </summary>
        public Guid IdentityGuid { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// ID роли пользователя
        /// </summary>
        public Role Role { get; set; }
        public int RoleId { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronimyc { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Признак действия записи пользователя
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Список курсов пользователя
        /// </summary>
        public List<UserCourse> CourseUsers { get; set; } = new List<UserCourse>();

        /// <summary>
        /// Список организаций в которых участвует пользователь
        /// </summary>
        public Organization Organization { get; set; }

        public int? OrganizationId { get; set; }

    }
}
