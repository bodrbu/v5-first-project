﻿namespace Core.Domain.PersonalInformation
{
    /// <summary>
    /// Успеваемость пользователя
    /// </summary>
    public class AcademicPerformance
        : BaseEntity
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Успеваемость пользователя
        /// </summary>
        public string Raiting { get; set; }
    }
}
