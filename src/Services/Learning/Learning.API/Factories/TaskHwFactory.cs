﻿using Core.Domain.Courses;
using Learning.API.Models.HomeWorks;


namespace Learning.API.Factories
{
    public static class TaskHwFactory
    {
        public static HomeWorkTask FromDto(CreateTaskRequest request)
        {
            var hw = new HomeWorkTask
            {
                CourseId = request.CourseId,
                Name = request.Name,
                Description = request.Description,
                Answer = request.Answer
            };
            return hw;
        }
    }
}