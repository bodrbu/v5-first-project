﻿using System.Collections.Generic;
using Core.Domain.Courses;
using Learning.API.Models.Courses;

#pragma warning disable 1591

namespace Learning.API.Factories
{
    public static class LessonFactory
    {
        public static Lesson FromRequest(CreateLessonRequest createLessonRequest)
        {
            return new Lesson
            {
                Title = createLessonRequest.Name,
                Description = createLessonRequest.Description,
                VideoUri = createLessonRequest.VideoUri,
                Comments = new List<Comment>()
            };
        }
    }
}