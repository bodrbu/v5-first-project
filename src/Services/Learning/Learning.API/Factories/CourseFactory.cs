﻿using System;
using Core.Domain.Courses;
using Learning.API.Models.Courses;

#pragma warning disable 1591

namespace Learning.API.Factories
{
    public static class CourseFactory
    {
        public static Course CreateFromDraftRequest(CourseDraftRequest courseDto)
        {
            var course = new Course()
            {
                Name = courseDto.Name,
                Description = courseDto.Description,
                OrganizationOwnerId = courseDto.OrganizationId,
                CreationDate = DateTime.Now,
                UserOwnerId = courseDto.UserId,
                IsDraft = true
            };
            return course;
        }
    }
}