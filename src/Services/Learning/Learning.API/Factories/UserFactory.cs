﻿using System;
using Core.Domain.PersonalInformation;
using Learning.API.Models.Users;

#pragma warning disable 1591

namespace Learning.API.Factories
{
    public class UserFactory
    {
        public static User FromDto(UserDto dto)
        {
            return new User
            {
                Login = dto.Login,
                Birthdate = dto.BDay,
                Name = dto.Name,
                Surname = dto.Surname,
                Patronimyc = dto.Patronimyc,
                IsActive = true,
                DateCreate = DateTime.Now,
                DateModified = DateTime.Now,
                Password = dto.Password,
                OrganizationId = dto.OrganizationId,
                RoleId = dto.RoleId,
            };
        }
    }
}