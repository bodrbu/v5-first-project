﻿using System.Linq;
using Core.Domain.Courses;
using Core.Domain.PersonalInformation;
using Learning.API.Models.Courses;
using Learning.API.Models.HomeWorks;
using Learning.API.Models.Users;
using Mapster;

#pragma warning disable 1591

namespace Learning.API.Factories
{
    public static class CourseExtensions
    {
        public static CourseResponse ToCourseDto(this Course course)
        {
            return new CourseResponse()
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                OrganizationOwner = new OrganizationResponse()
                {
                    Id = course.OrganizationOwner.Id,
                    Name = course.OrganizationOwner.Name,
                    Description = course.OrganizationOwner.Description
                },
                UserId = course.UserOwnerId,
                Lessons = course.Lessons.Select(lesson => lesson.ToLessonShortDto()),
                Tasks = course.Tasks.Select(t => t.Adapt<TaskShortDto>()),
                isDraft = course.IsDraft
            };
        }

        public static CourseShortResponse ToCourseShortDto(this Course course)
        {
            return new CourseShortResponse()
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                OrganizationOwnerId = course.OrganizationOwnerId,
                UserOwnerId = course.UserOwnerId,
                isDraft = course.IsDraft
            };
        }
    }

    public static class LessonExtensions
    {
        public static LessonShortResponse ToLessonShortDto(this Lesson lesson)
        {
            return new LessonShortResponse {Id = lesson.Id, Name = lesson.Title};
        }

        public static LessonResponse ToLessonDto(this Lesson lesson)
        {
            return new LessonResponse
            {
                Id = lesson.Id,
                Name = lesson.Title,
                Description = lesson.Description,
                VideoUri = lesson.VideoUri,
                Comments = lesson.Comments.Select(comment => comment.ToCommentDto()),
                CourseId = lesson.CourseId
            };
        }

        public static CommentResponse ToCommentDto(this Comment comment)
        {
            return new CommentResponse
            {
                Id = comment.Id,
                CreationDate = comment.CreationDate.ToString("yyyy-MM-dd"),
                Message = comment.Message,
                UserId = comment.PublisherId,
                LessonId = comment.LessonId
            };
        }
    }

    public static class OrganizationExtensions
    {
        public static OrganizationResponse ToOrganizationDto(this Organization org)
        {
            return new OrganizationResponse()
            {
                Id = org.Id,
                Name = org.Name,
                Description = org.Description
            };
        }
    }

    public static class RoleExtensions
    {
        public static RoleDto ToRoleDto(this Role role)
        {
            return new RoleDto
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description,
            };
        }
    }

    public static class UserExtensions
    {
        public static UserDto ToDto(this User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Login = user.Login,
                Name = user.Name,
                Surname = user.Surname,
                Patronimyc = user.Patronimyc,
                BDay = user.Birthdate,
                OrganizationId = user.OrganizationId ?? 0,
                DateCreate = user.DateCreate,
                DateModified = user.DateModified,
                RoleId = user.RoleId,
                IdentityGuid = user.IdentityGuid.ToString("D")
            };
        }

        public static UserShortDto ToShortDto(this User user)
        {
            return new UserShortDto
            {
                Id = user.Id,
                IdentityGuid = user.IdentityGuid.ToString("D"),
                Login = user.Login
            };
        }
    }
}