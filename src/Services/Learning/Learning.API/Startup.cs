using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using Core.Abstractions;
using DataAccess.Data;
using FluentValidation.AspNetCore;
using Learning.API.Infrastructure.Filters;
using Learning.API.Options;
using Learning.API.Services;
using Learning.API.Services.Abstractions;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using NSwag;
using NSwag.AspNetCore;
using NSwag.Generation.Processors.Security;
using Serilog;

#pragma warning disable 1591

namespace Learning.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:5000").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    });
            });


            services
                .AddControllers(options =>
                {
                    options.SuppressAsyncSuffixInActionNames = false;
                    options.Filters.Add<DomainExceptionsFilter>();
                })
                .AddFluentValidation(fv => { fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()); });

            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");
            if (Environment.IsDevelopment())
            {
                IdentityModelEventSource.ShowPII = true;
            }
            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            var identityExternalUrl = Configuration.GetValue<string>("IdentityExternalUrl");
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = identityUrl;
                options.RequireHttpsMetadata = false;
                options.Audience = "learning";
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "EduForm.Learning API";
                options.Version = "1.0";
                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = $"{identityExternalUrl}/connect/authorize",
                            TokenUrl = $"{identityExternalUrl}/connect/token",
                            Scopes = new Dictionary<string, string>
                            {
                                { "learning", "Learning Service" }
                            }
                        }
                    }
                });
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });

            AddCustomAuthorization(services);

            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<ILessonService, LessonService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IOrganizationService, OrganizationService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddScoped<IInitialize, EfDbInitializer>();

            services.AddDbContext<AppDbcontext>(x =>
            {
                x.UseNpgsql(Configuration.GetValue<string>("ConnectionString"),
                    option => { option.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName); });
                if (Debugger.IsAttached)
                {
                    x.EnableSensitiveDataLogging();
                }
            });

            ConfigureMassTransitServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IInitialize dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else if (env.IsProduction())
            {
                app.UseExceptionHandler("/error");
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
                x.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "learningswaggerui",
                    AppName = "Learning Swagger UI",
                };
            });

            app.UseRouting();
            app.UseSerilogRequestLogging();

            app.UseCors(options =>
                options.AllowAnyOrigin().AllowAnyMethod()
                    .AllowAnyHeader()); //.WithOrigins(Configuration.GetValue<string>("SpaHostUrl")));

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization("ApiScope");
            });

            dbInitializer.Initialize();
        }

        public void ConfigureMassTransitServices(IServiceCollection services)
        {
            var opts = Configuration.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();
            services.AddMassTransit(x =>
            {
                //EndpointConvention.Map<AddUserPerformance>(new Uri("queue:performance-service"));

                x.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(opts.Host, opts.Port, opts.VirtualHost, h =>
                    {
                        h.Username("guest");
                        h.Password("guest");
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        public static IServiceCollection AddCustomAuthorization( IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", builder =>
                {
                    builder.RequireAuthenticatedUser();
                    builder.RequireClaim("scope", "learning");
                });
            });
            return services;
        }
    }
}