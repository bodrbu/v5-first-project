﻿using System;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Http;

namespace Learning.API.Services
{
    public class IdentityService: IIdentityService
    {
        private readonly IHttpContextAccessor _context;

        public IdentityService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string GetUserIdentity()
        {
            return _context.HttpContext.User.FindFirst("sub").Value;
        }
    }
}