﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

#pragma warning disable 1591

namespace Learning.API.Services
{
    public class CourseService : ICourseService
    {
        private readonly AppDbcontext _context;

        public CourseService(AppDbcontext context)
        {
            _context = context;
        }

        public Task<IEnumerable<CourseShortResponse>> GetAllAsync()
        {
            var courses = _context.Courses
                .AsNoTracking()
                .Include(course => course.OrganizationOwner)
                .Select(course => course.ToCourseShortDto())
                .AsEnumerable();
            return Task.FromResult(courses);
        }

        /// <inheritdoc />
        public async Task<CourseResponse> GetByIdAsync(int id)
        {
            var course = await _context.Courses
                .Include(c => c.OrganizationOwner)
                .Include(c => c.UserOwner)
                .Include(c => c.Tasks)
                .Include(c => c.Lessons)
                .ThenInclude(lesson => lesson.Comments)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (course is null)
                throw new EntityNotFoundDomainException($"Курс с Id = {id} не найден");

            return course.ToCourseDto();
        }

        public Task<IEnumerable<CourseShortResponse>> GetByOwnerUserIdAsync(int userId)
        {
            var courses = _context.Courses
                .AsNoTracking()
                .Include(course => course.OrganizationOwner)
                .Where(course => course.UserOwnerId == userId)
                .Select(course => course.ToCourseShortDto())
                .AsEnumerable();
            return Task.FromResult(courses);
        }

        public async Task<IEnumerable<CourseShortResponse>> GetWithUserIdAsync(int userId)
        {
            var uId = await _context.Users
                .AsNoTracking()
                .Select(user => user.Id)
                .SingleOrDefaultAsync(id => id == userId);

            if (uId is 0)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {userId} не найден");
            }

            var courses = _context.Courses
                .AsNoTracking()
                .Where(course => course.CourseUsers.Select(userCourse => userCourse.UserId).Contains(userId))
                .Select(course => course.ToCourseShortDto())
                .AsEnumerable();

            return courses;
        }

        public async Task<int> CreateDraftAsync(CourseDraftRequest courseDto)
        {
            var course = CourseFactory.CreateFromDraftRequest(courseDto);
            await _context.Courses.AddAsync(course);
            await _context.SaveChangesAsync();
            return course.Id;
        }

        public async Task PublishCourseAsync(int id)
        {
            var course = await _context.Courses
                .Include(c => c.Lessons)
                .SingleOrDefaultAsync(c => c.Id == id);

            if (course is null)
            {
                throw new EntityNotFoundDomainException($"Не найден курс с Id = {id}");
            }

            if (course.IsDraft == false)
            {
                throw new EduFormException("Курс уже опубликован");
            }

            //Проверить выполнение бизнес-правил
            if (!course.Lessons.Any())
            {
                throw new EduFormException("Нельзя опубликовать курс без занятий!");
            }

            //Изменить статус черновика
            course.IsDraft = false;

            _context.Update(course);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(UpdateCourseRequest courseDto)
        {
            var course = await _context.Courses
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Id == courseDto.Id);
            if (course is null)
            {
                throw new EntityNotFoundDomainException($"Курс с id = {courseDto.Id} не существует");
            }

            if (courseDto.UserId != course.UserOwnerId)
            {
                throw new EduFormException("Вам не позволено редактировать данный курс");
            }

            course.Name = courseDto.Name;
            course.Description = courseDto.Description;
            //course.OrganizationOwnerId = courseDto.OrganizationId;

            _context.Courses.Update(course);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var course = await _context.Courses.FirstOrDefaultAsync(c => c.Id == id);
            if (course is null)
            {
                throw new EntityNotFoundDomainException($"Курс с id={id} не существует");
            }

            _context.Remove(course);
            await _context.SaveChangesAsync();
        }
    }
}