﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Courses;
using Core.Domain.Exceptions;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Learning.API.Services
{
    public class CommentService : ICommentService
    {
        private readonly AppDbcontext _context;

        public CommentService(AppDbcontext context)
        {
            _context = context;
        }

        public async Task<CommentResponse> GetAsync(int id)
        {
            var commentDto = await _context.Comments.SingleOrDefaultAsync(comment => comment.Id == id);
            if (commentDto is null)
            {
                throw new EntityNotFoundDomainException($"Сущность с Id = {id} не найдена"); 
            }

            return commentDto.ToCommentDto();
        }

        public async Task<IEnumerable<CommentResponse>> GetLessonCommentsIdAsync(int lessonId)
        {
            var lesson = await _context.Lessons
                .SingleOrDefaultAsync(l => l.Id == lessonId);

            if (lesson is null)
            {
                throw new EntityNotFoundDomainException($"Урок с Id = {lessonId} не найден");
            }

            var comments = _context.Comments
                .Where(comment => comment.LessonId == lessonId)
                .Select(comment => comment.ToCommentDto());
            
            return comments;
        }

        public async Task<int> AddCommentAsync(CommentResponse commentResponse)
        {
            var comment = new Comment
            {
                LessonId = commentResponse.LessonId,
                PublisherId = commentResponse.UserId,
                Message = commentResponse.Message,
                CreationDate = DateTime.Now,
                DateModified = DateTime.Now,
            };
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
            return comment.Id;
        }
    }
}