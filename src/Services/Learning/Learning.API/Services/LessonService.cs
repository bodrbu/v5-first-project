﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Learning.API.Services
{
    public class LessonService : ILessonService
    {
        private readonly AppDbcontext _context;

        public LessonService(AppDbcontext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<LessonShortResponse>> GetCourseLessonsAsync(int courseId)
        {
            var course = await _context.Courses
                .Include(c => c.Lessons)
                .SingleOrDefaultAsync(c => c.Id == courseId);

            if (course is null)
            {
                throw new EntityNotFoundDomainException($"Курс с Id = {courseId} не найден");
            }

            var lessonDtos = course.Lessons.Select(lesson => lesson.ToLessonShortDto());
            return lessonDtos;
        }

        public async Task<LessonResponse> GetByIdAsync(int lessonId)
        {
            var lesson = await _context.Lessons
                .Include(l => l.Comments)
                .SingleOrDefaultAsync(l => l.Id == lessonId);

            if (lesson is null)
            {
                throw new EntityNotFoundDomainException($"Урок с Id = {lessonId} не найден");
            }

            return lesson.ToLessonDto();
        }

        public async Task<int> AddLessonAsync(CreateLessonRequest createLessonDto)
        {
            var course = await _context.Courses
                .Include(c => c.Lessons)
                .SingleOrDefaultAsync(c => c.Id == createLessonDto.CourseId);
            if (course == null) throw new EntityNotFoundDomainException($"Курс с id = {createLessonDto.CourseId} не существует");

            var lesson = LessonFactory.FromRequest(createLessonDto);
            course.Lessons.Add(lesson);

            _context.Update(course);
            await _context.SaveChangesAsync();

            return lesson.Id;
        }

        public async Task UpdateLessonAsync(UpdateLessonRequest updateLessonRequest)
        {
            var lesson = await _context.Lessons.SingleOrDefaultAsync(l => l.Id == updateLessonRequest.Id);

            lesson.Description = updateLessonRequest.Description;
            lesson.Title = updateLessonRequest.Name;
            lesson.VideoUri = updateLessonRequest.VideoUri;

            _context.Update(lesson);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveLessonAsync(int lessonId)
        {
            var lesson = await _context.Lessons.SingleOrDefaultAsync(l => l.Id == lessonId);

            if (lesson is null)
            {
                throw new EntityNotFoundDomainException($"Занятие сId = {lessonId} не найдено");
            }

            _context.Remove(lesson);
            await _context.SaveChangesAsync();
        }
    }
}