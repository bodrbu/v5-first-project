﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Courses;
using Core.Domain.Exceptions;
using Core.Domain.PersonalInformation;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Courses;
using Learning.API.Models.Users;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Learning.API.Services
{
    class UserService : IUserService
    {
        private readonly AppDbcontext _context;

        public UserService(AppDbcontext context)
        {
            _context = context;
        }

        public Task<IEnumerable<UserShortDto>> GetAllAsync()
        {
            var users = _context.Users
                .AsNoTracking()
                .Select(user => user.ToShortDto())
                .AsEnumerable();
            return Task.FromResult(users);
        }

        public Task<IEnumerable<UserShortDto>> GetWithCourse(int courseId)
        {
            var users = _context.Users
                .AsNoTracking()
                .Where(user => user.CourseUsers.Select(userCourse => userCourse.CourseId).Contains(courseId))
                .Select(user => user.ToShortDto())
                .AsEnumerable();

            return Task.FromResult(users);
        }

        public Task<IEnumerable<CourseShortResponse>> GetUserCoursesAsync(int userId)
        {
            var courses = _context.Courses
                .AsNoTracking()
                .Where(course => course.CourseUsers.Select(userCourse => userCourse.UserId).Contains(userId))
                .Select(course => course.ToCourseShortDto())
                .AsEnumerable();

            return Task.FromResult(courses);
        }

        public async Task<IEnumerable<UserShortDto>> GetWithOrganizationAsync(int organizationId)
        {
            var orgId = await _context.Organizations.Select(organization => organization.Id)
                .SingleOrDefaultAsync(id => id == organizationId);
            if (orgId == default)
            {
                throw new EntityNotFoundDomainException($"Организации с Id = {organizationId} не существует");
            }

            var users = _context.Users
                .AsNoTracking()
                .Where(user => user.OrganizationId == organizationId)
                .Select(user => user.ToShortDto())
                .AsEnumerable();

            return users;
        }

        public async Task<UserDto> GetByIdAsync(int id)
        {
            var user = await _context.Users
                .Include(u => u.Role)
                .SingleOrDefaultAsync(l => l.Id == id);
            if (user is null)
            {
                throw new EntityNotFoundDomainException($"Урок с Id = {id} не найден");
            }
            var userDto = user.ToDto();

            return userDto;
        }

        public async Task<UserDto> GetByIdentityAsync(Guid identityGuid)
        {
            var user = await _context.Users
                .Include(u => u.Role)
                .SingleOrDefaultAsync(l => l.IdentityGuid == identityGuid);
            if (user is null)
            {
                throw new EntityNotFoundDomainException($"Урок с IdentityGuid = {identityGuid} не найден");
            }
            var userDto = user.ToDto();

            return userDto;
        }

        public async Task<int> CreateAsync(UserDto dto)
        {
            var user = UserFactory.FromDto(dto);

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return user.Id;
        }

        public async Task AddToOrgAsync(int userId, int organizationId)
        {
            var orgId = await _context.Organizations.Select(organization => organization.Id)
                .SingleOrDefaultAsync(id => id == organizationId);
            if (orgId == default)
            {
                throw new EntityNotFoundDomainException($"Организации с Id = {organizationId} не существует");
            }

            var user = await _context.Users
                .SingleOrDefaultAsync(l => l.Id == userId);
            if (user is null)
            {
                throw new EntityNotFoundDomainException($"Урок с Id = {userId} не найден");
            }

            user.OrganizationId = organizationId;
            await _context.SaveChangesAsync();
        }

        public async Task AddToCourseAsync(int userId, int courseId)
        {
            var cId = await _context.Courses.Select(course => course.Id)
                .SingleOrDefaultAsync(id => id == courseId);
            if (cId == default)
            {
                throw new EntityNotFoundDomainException($"Курса с Id = {courseId} не существует");
            }

            var userCheckedId = await _context.Users
                .Select(user => user.Id)
                .SingleOrDefaultAsync(id => id == userId);
            if (userCheckedId == default)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {userCheckedId} не найден");
            }

            var userCourse = new UserCourse
            {
                UserId = userId,
                CourseId = courseId
            };

            await _context.AddAsync(userCourse);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(UserDto dto)
        {
            var user = await _context.Users
                .SingleOrDefaultAsync(usr => usr.Id == dto.Id);

            if (user is null)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {dto.Id} не существует");
            }

            user.Login = dto.Login;
            user.Birthdate = dto.BDay;
            user.Name = dto.Name;
            user.Surname = dto.Surname;
            user.Patronimyc = dto.Patronimyc;
            user.DateModified = DateTime.Now;
            user.Password = dto.Password;
        }

        public async Task DeleteAsync(int id)
        {
            var userId = await _context.Users
                .AsNoTracking()
                .Select(user => user.Id)
                .SingleOrDefaultAsync(i => i == id);

            if (userId == default)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {userId} не существует");
            }

            _context.Remove(new User {Id = userId});
            await _context.SaveChangesAsync();
        }

        public async Task RemoveFromOrgAsync(int userId, int organizationId)
        {
            var orgId = await _context.Organizations.Select(organization => organization.Id)
                .SingleOrDefaultAsync(id => id == organizationId);
            if (orgId == default)
            {
                throw new EntityNotFoundDomainException($"Организации с Id = {organizationId} не существует");
            }

            var user = await _context.Users
                .SingleOrDefaultAsync(l => l.Id == userId);
            if (user is null)
            {
                throw new EntityNotFoundDomainException($"Урок с Id = {userId} не найден");
            }

            user.OrganizationId = default;
            await _context.SaveChangesAsync();
        }

        public async Task RemoveFromCourseAsync(int userId, int courseId)
        {
            var cId = await _context.Courses.Select(course => course.Id)
                .SingleOrDefaultAsync(id => id == courseId);
            if (cId == default)
            {
                throw new EntityNotFoundDomainException($"Курса с Id = {courseId} не существует");
            }

            var userCheckedId = await _context.Users
                .Select(user => user.Id)
                .SingleOrDefaultAsync(id => id == userId);
            if (userCheckedId == default)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {userCheckedId} не найден");
            }

            // var uc = await _context.UserCourse
            //     .SingleOrDefaultAsync(course => course.CourseId == courseId && course.UserId == userId);

            var userCourse = new UserCourse
            {
                UserId = userId,
                CourseId = courseId
            };

            _context.Remove(userCourse);
            await _context.SaveChangesAsync();
        }
    }
}