﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Courses;
using Core.Domain.Exceptions;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Learning.API.Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly AppDbcontext _context;

        public OrganizationService(AppDbcontext context)
        {
            _context = context;
        }

        public Task<IEnumerable<OrganizationResponse>> GetAllAsync()
        {
            var orgs = _context.Organizations
                .AsNoTracking()
                .Select(org => org.ToOrganizationDto())
                .AsEnumerable();
            return Task.FromResult(orgs);
        }

        public async Task<OrganizationResponse> GetByIdAsync(int organizationId)
        {
            var org = await _context.Organizations
                .AsNoTracking()
                .FirstOrDefaultAsync(organization => organization.Id == organizationId);

            if (org is null)
            {
                throw new EntityNotFoundDomainException($"Организация с id = {organizationId} не существует");
            }

            return org.ToOrganizationDto();
        }

        public async Task<IEnumerable<CourseShortResponse>> GetOrganizationCoursesAsync(int organizationId)
        {
            var orgId = await _context.Organizations
                .AsNoTracking()
                .Select(o => o.Id)
                .FirstOrDefaultAsync(id => id == organizationId);

            if (orgId == 0)
            {
                throw new EntityNotFoundDomainException($"Организация с id = {organizationId} не существует");
            }

            var courses = _context.Courses
                .AsNoTracking()
                .Include(course => course.OrganizationOwner)
                .Where(course => course.OrganizationOwner.Id == organizationId)
                .Select(course => course.ToCourseShortDto())
                .AsEnumerable();
            return courses;
        }

        public async Task<OrganizationResponse> GetByCourseIdAsync(int courseId)
        {
            var org = await _context.Courses
                .Include(course => course.OrganizationOwner)
                .Where(course => course.Id == courseId)
                .Select(course => course.OrganizationOwner.ToOrganizationDto())
                .AsNoTracking()
                .SingleOrDefaultAsync();

            if (org is null)
                throw new EntityNotFoundDomainException($"Не найден курс с Id = {courseId}");

            return org;
        }

        public async Task<int> AddAsync(OrganizationResponse organization)
        {
            var org = new Organization()
            {
                Name = organization.Name,
                Description = organization.Description,
                DateCreate = DateTime.Now
            };
            await _context.Organizations.AddAsync(org);
            await _context.SaveChangesAsync();
            return org.Id;
        }

        public async Task UpdateAsync(OrganizationResponse organization)
        {
            var org = await _context.Organizations
                .SingleOrDefaultAsync(o => o.Id == organization.Id);
            if (org is null)
            {
                throw new EntityNotFoundDomainException($"Организация с id = {organization.Id} не существует");
            }

            org.Name = organization.Name;
            org.Description = organization.Description;
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(int organizationId)
        {
            var org = await _context.Organizations
                .SingleOrDefaultAsync(o => o.Id == organizationId);
            if (org is null)
            {
                throw new EntityNotFoundDomainException($"Организация с id = {organizationId} не существует");
            }

            _context.Remove(org);
            await _context.SaveChangesAsync();
        }
    }
}