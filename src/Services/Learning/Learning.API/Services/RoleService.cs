﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using Core.Domain.PersonalInformation;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.Users;
using Learning.API.Services.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Learning.API.Services
{
    public class RoleService : IRoleService
    {
        private readonly AppDbcontext _context;

        public RoleService(AppDbcontext context)
        {
            _context = context;
        }

        public Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var roles = _context.Roles
                .AsNoTracking()
                .Select(role => role.ToRoleDto())
                .AsEnumerable();
            return Task.FromResult(roles);
        }

        public async Task<RoleDto> GetByIdAsync(int roleId)
        {
            var r = await _context.Roles
                .AsNoTracking()
                .FirstOrDefaultAsync(role => role.Id == roleId);

            if (r is null)
            {
                throw new EntityNotFoundDomainException($"Роль с Id = {roleId} не найдена");
            }
            
            return r.ToRoleDto();
        }

        public async Task<int> AddAsync(RoleDto roleDto)
        {
            var role = new Role
            {
                Name = roleDto.Name,
                Description = roleDto.Description,
                DateCreate = DateTime.Now,
                DateModified = DateTime.Now
            };
            await _context.AddAsync(role);
            await _context.SaveChangesAsync();
            return role.Id;
        }

        public async Task UpdateAsync(RoleDto role)
        {
            var r = await _context.Roles.FirstOrDefaultAsync(p => p.Id == role.Id);
            if (r is null)
            {
                throw new EntityNotFoundDomainException($"Не найдена роль с Id = {role.Id}");
            }

            r.Name = role.Name;
            r.Description = role.Description;
            r.DateModified = DateTime.Now;

            _context.Update(r);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(int roleId)
        {
            var role = await _context.Roles.FirstOrDefaultAsync(r => r.Id == roleId);
            if (role is null)
            {
                throw new EntityNotFoundDomainException($"Не найдена роль с Id = {roleId}");
            }
            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();
        }
    }
}