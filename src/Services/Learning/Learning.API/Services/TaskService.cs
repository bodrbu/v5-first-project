﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain.Exceptions;
using DataAccess.Data;
using Learning.API.Factories;
using Learning.API.Models.HomeWorks;
using Learning.API.Services.Abstractions;
using Mapster;
using Microsoft.EntityFrameworkCore;


namespace Learning.API.Services
{
    public class TaskService : ITaskService
    {
        private readonly AppDbcontext _context;

        public TaskService(AppDbcontext context)
        {
            _context = context;
        }

        public async Task<TaskResponse> GetByIdAsync(int taskId)
        {
            var t = await _context.TaskHWs
                .AsNoTracking()
                .SingleOrDefaultAsync(work => work.Id == taskId);

            if (t is null)
            {
                throw new EntityNotFoundDomainException($"Домашнее задание с Id = {taskId} не существует");
            }

            return t.Adapt<TaskResponse>();
        }

        public async Task<IEnumerable<TaskShortDto>> GetHomeworksByCourseAsync(int courseId)
        {
            var cId = await _context.Courses
                .AsNoTracking()
                .Select(course => course.Id)
                .SingleOrDefaultAsync(id => id == courseId);

            if (cId is 0)
            {
                throw new EntityNotFoundDomainException($"Пользователь с Id = {courseId} не найден");
            }

            var homeWorkShortDtos = _context.TaskHWs
                .AsNoTracking()
                .Where(work => work.CourseId == courseId)
                .Select(work => work.Adapt<TaskShortDto>())
                .AsEnumerable();

            return homeWorkShortDtos;
        }

        public async Task<int> AddAsync(CreateTaskRequest createTaskRequest)
        {
            var course = await _context.Courses
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Id == createTaskRequest.CourseId);
            if (course is null)
            {
                throw new EntityNotFoundDomainException($"Курс с Id = {createTaskRequest.CourseId} не существует");
            }

            var hw = TaskHwFactory.FromDto(createTaskRequest);

            await _context.TaskHWs.AddAsync(hw);
            await _context.SaveChangesAsync();

            return hw.Id;
        }

        public async Task<bool> TrySolveAsync(SolveTaskRequest solveTaskRequest)
        {
            var task = await _context.TaskHWs
                .SingleOrDefaultAsync(work => work.Id == solveTaskRequest.TaskId);

            if (task is null)
            {
                throw new EntityNotFoundDomainException($"Задача с Id = {solveTaskRequest.TaskId} не найдена");
            }

            return string.Compare(task.Answer, solveTaskRequest.Answer, StringComparison.InvariantCultureIgnoreCase) == 0;
        }

        public async Task UpdateAsync(UpdateTaskRequest request)
        {
            var task = await _context.TaskHWs
                .SingleOrDefaultAsync(work => work.Id == request.Id);

            if (task is null)
            {
                throw new EntityNotFoundDomainException($"Задача с Id = {request.Id} не найдена");
            }

            task.Name = request.Name;
            task.Description = request.Description;
            task.Answer = request.Answer;

            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(int id)
        {
            var hw = await _context.TaskHWs
                .SingleOrDefaultAsync(work => work.Id == id);

            if (hw is null)
            {
                throw new EntityNotFoundDomainException($"Домашняя работа с Id = {id} не найдена");
            }

            _context.Remove(hw);
            await _context.SaveChangesAsync();
        }
    }
}