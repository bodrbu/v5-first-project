﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;

#pragma warning disable 1591
namespace Learning.API.Services.Abstractions
{
    public interface ILessonService
    {
        Task<IEnumerable<LessonShortResponse>> GetCourseLessonsAsync(int courseId);
        Task<LessonResponse> GetByIdAsync(int lessonId);
        Task<int> AddLessonAsync(CreateLessonRequest createLessonDto);
        Task UpdateLessonAsync(UpdateLessonRequest updateLessonRequest);
        Task RemoveLessonAsync(int lessonId);
    }
}