﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Users;

#pragma warning disable 1591
namespace Learning.API.Services.Abstractions
{
    public interface IRoleService
    {
        Task<IEnumerable<RoleDto>> GetAllAsync();
        Task<RoleDto> GetByIdAsync(int roleId);
        Task<int> AddAsync(RoleDto role);
        Task UpdateAsync(RoleDto role);
        Task RemoveAsync(int roleId);
    }
}