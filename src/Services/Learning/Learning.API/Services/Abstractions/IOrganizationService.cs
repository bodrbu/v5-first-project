﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;

#pragma warning disable 1591
namespace Learning.API.Services.Abstractions
{
    public interface IOrganizationService
    {
        Task<IEnumerable<OrganizationResponse>> GetAllAsync();
        Task<OrganizationResponse> GetByIdAsync(int organizationId);
        Task<IEnumerable<CourseShortResponse>> GetOrganizationCoursesAsync(int organizationId);
        Task<OrganizationResponse> GetByCourseIdAsync(int courseId);
        Task<int> AddAsync(OrganizationResponse organization);
        Task UpdateAsync(OrganizationResponse organization);
        Task RemoveAsync(int organizationId);
    }
}