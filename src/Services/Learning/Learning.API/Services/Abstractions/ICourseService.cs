﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;

#pragma warning disable 1591

namespace Learning.API.Services.Abstractions
{
    public interface ICourseService
    {
        Task<IEnumerable<CourseShortResponse>> GetAllAsync();
        Task<CourseResponse> GetByIdAsync(int id);
        Task<IEnumerable<CourseShortResponse>> GetByOwnerUserIdAsync(int userId);
        Task<IEnumerable<CourseShortResponse>> GetWithUserIdAsync(int userId);
        Task<int> CreateDraftAsync(CourseDraftRequest courseDto);
        Task PublishCourseAsync(int id);
        Task UpdateAsync(UpdateCourseRequest courseDto);
        Task DeleteAsync(int id);
    }
}