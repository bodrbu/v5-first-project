﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.HomeWorks;

#pragma warning disable 1591
namespace Learning.API.Services.Abstractions
{
    public interface ITaskService
    {
        Task<TaskResponse> GetByIdAsync(int taskId);
        Task<IEnumerable<TaskShortDto>> GetHomeworksByCourseAsync(int courseId);
        Task<int> AddAsync(CreateTaskRequest createTaskRequest);
        Task<bool> TrySolveAsync(SolveTaskRequest solveTaskRequest);
        Task UpdateAsync(UpdateTaskRequest request);
        Task RemoveAsync(int id);
    }
}