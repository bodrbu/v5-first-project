﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;
using Learning.API.Models.Users;

#pragma warning disable 1591
namespace Learning.API.Services.Abstractions
{
    public interface IUserService
    {
        Task<IEnumerable<UserShortDto>> GetAllAsync();
        Task<IEnumerable<UserShortDto>> GetWithCourse(int courseId);
        Task<IEnumerable<CourseShortResponse>> GetUserCoursesAsync(int userId);
        Task<IEnumerable<UserShortDto>> GetWithOrganizationAsync(int organizationId);
        Task<UserDto> GetByIdAsync(int id);
        Task<UserDto> GetByIdentityAsync(Guid identityGuid);
        Task<int> CreateAsync(UserDto dto);
        Task AddToOrgAsync(int userId, int organizationId);
        Task AddToCourseAsync(int userId, int courseId);
        Task UpdateAsync(UserDto dto);
        Task DeleteAsync(int id);
        Task RemoveFromOrgAsync(int userId, int organizationId);
        Task RemoveFromCourseAsync(int userId, int courseId);
    }
}