﻿namespace Learning.API.Services.Abstractions
{
    public interface IIdentityService
    {
        string GetUserIdentity();
    }
}