﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;

namespace Learning.API.Services.Abstractions
{
    public interface ICommentService
    {
        Task<CommentResponse> GetAsync(int id);
        Task<IEnumerable<CommentResponse>> GetLessonCommentsIdAsync(int lessonId);
        Task<int> AddCommentAsync(CommentResponse commentResponse);
    }
}