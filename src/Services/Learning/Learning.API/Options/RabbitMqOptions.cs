﻿// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Learning.API.Options
{
    public class RabbitMqOptions
    {
        public const string SectionName = nameof(RabbitMqOptions);

        public string Host { get; set; }
        public ushort Port { get; set; }
        public string VirtualHost { get; set; }
    }
}