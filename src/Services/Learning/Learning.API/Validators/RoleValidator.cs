﻿using FluentValidation;
using Learning.API.Models.Users;

namespace Learning.API.Validators
{
    public class RoleValidator : AbstractValidator<RoleDto>
    {
        public RoleValidator()
        {
            RuleFor(p => p.Name).NotEmpty().WithMessage("Имя не может быть пустым");
            RuleFor(p => p.Description).NotEmpty().WithMessage("Описание не может быть пустым");
        }
    }
}