﻿using System;
using FluentValidation;
using Learning.API.Models.Courses;

namespace Learning.API.Validators
{
    public class LessonValidator : AbstractValidator<CreateLessonRequest>
    {
        public LessonValidator()
        {
            RuleFor(l => l.Name).NotEmpty().WithMessage("Имя не может быть пустым");
            RuleFor(request => request.Description).NotEmpty().WithMessage("Описание не может быть пустым");
            RuleFor(l => l.VideoUri)
                .NotEmpty()
                .Must(s => Uri.IsWellFormedUriString(s, UriKind.RelativeOrAbsolute));
        }
    }
}