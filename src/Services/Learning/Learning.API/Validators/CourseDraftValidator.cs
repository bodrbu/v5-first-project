﻿using FluentValidation;
using Learning.API.Models.Courses;

namespace Learning.API.Validators
{
    public class CourseDraftValidator : AbstractValidator<CourseDraftRequest>
    {
        public CourseDraftValidator()
        {
            RuleFor(p => p.Name).NotNull().WithMessage("Имя курса не может быть пустым");
            RuleFor(p => p.Description).NotNull().WithMessage("Описание курса не может быть пустым");
            RuleFor(p => p.UserId)
                .NotNull()
                .GreaterThan(0)
                .WithMessage("Укажите ненулевой идентификатор создателя курса");
            RuleFor(p => p.OrganizationId)
                .NotNull();
        }
    }
}