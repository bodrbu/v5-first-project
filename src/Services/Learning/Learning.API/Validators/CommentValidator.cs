﻿using FluentValidation;
using Learning.API.Models.Courses;

namespace Learning.API.Validators
{
    public class CommentValidator : AbstractValidator<CommentResponse>
    {
        public CommentValidator()
        {
            RuleFor(dto => dto.Message).NotEmpty();
            RuleFor(dto => dto.CreationDate).NotEmpty();
            RuleFor(dto => dto.LessonId).NotEmpty();
            RuleFor(dto => dto.UserId).NotEmpty();
        }
    }
}