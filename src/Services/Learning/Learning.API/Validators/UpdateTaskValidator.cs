﻿using FluentValidation;
using Learning.API.Models.HomeWorks;

namespace Learning.API.Validators
{
    public class UpdateTaskValidator : AbstractValidator<UpdateTaskRequest>
    {
        public UpdateTaskValidator()
        {
            RuleFor(p => p.Name).NotNull().WithMessage("Имя ДЗ не может быть пустым");
            RuleFor(p => p.Description).NotNull().WithMessage("Описание ДЗ не может быть пустым");
            RuleFor(p => p.Answer).NotNull().WithMessage("Правильный ответ не может быть пустым");
        }
    }
}