﻿using FluentValidation;
using Learning.API.Models.Users;

namespace Learning.API.Validators
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(dto => dto.Name).NotEmpty().WithMessage("Имя не может быть пустым");
            RuleFor(dto => dto.Surname).NotEmpty().WithMessage("Фамилия не может быть пустой");
            RuleFor(dto => dto.Patronimyc).NotEmpty().WithMessage("Отчество не может быть пустым");
            RuleFor(dto => dto.Login).NotEmpty().WithMessage("Логин не может быть пустым");
            RuleFor(dto => dto.Name).NotEmpty().WithMessage("Имя не может быть пустым");
            RuleFor(dto => dto.RoleId).NotNull();
            RuleFor(dto => dto.OrganizationId).GreaterThan(0).WithMessage("Id организации не должно быть > 0");
        }
    }
}