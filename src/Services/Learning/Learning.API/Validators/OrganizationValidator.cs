﻿using FluentValidation;
using Learning.API.Models.Courses;

namespace Learning.API.Validators
{
    public class OrganizationValidator : AbstractValidator<OrganizationResponse>
    {
        public OrganizationValidator()
        {
            RuleFor(dto => dto.Name).NotEmpty().WithMessage("Имя не может быть пустым");
            RuleFor(dto => dto.Description).NotEmpty().WithMessage("Описание не может быть пустым");
        }
    }
}