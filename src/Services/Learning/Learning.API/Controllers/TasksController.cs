﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using EduForm.Integration.Contracts;
using Learning.API.Models.HomeWorks;
using Learning.API.Services.Abstractions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    /// Домашние задания
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly IPublishEndpoint _publishEndpoint;

        /// <summary>
        /// Конструктор
        /// </summary>
        public TasksController(ITaskService taskService, IPublishEndpoint publishEndpoint)
        {
            _taskService = taskService;
            _publishEndpoint = publishEndpoint;
        }

        /// <summary>
        /// Получить домашнее задание по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор ДЗ</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(TaskResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<TaskResponse>> GetByIdAsync(int id)
        {
            var hw = await _taskService.GetByIdAsync(id);
            return Ok(hw);
        }

        /// <summary>
        /// Получить список задач выбранного курса
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор курса</param>
        [HttpGet("course/{id}")]
        [ProducesResponseType(typeof(IEnumerable<TaskShortDto>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<ActionResult<IEnumerable<TaskShortDto>>> GetCourseHomeworksAsync(int id)
        {
            var homeworks = await _taskService.GetHomeworksByCourseAsync(id);
            return Ok(homeworks);
        }

        /// <summary>
        /// Добавить задачу к курсу
        /// </summary>
        /// <returns></returns>
        /// <param name="createTaskRequest">Домашнее задание</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddTaskAsync([FromBody] CreateTaskRequest createTaskRequest)
        {
            var taskId = await _taskService.AddAsync(createTaskRequest);

            await _publishEndpoint.Publish<TaskAdded>(new
            {
                TaskId = taskId,
                CourseId = createTaskRequest.CourseId
            });

            return CreatedAtAction(nameof(GetByIdAsync), new {id = taskId}, null);
        }

        /// <summary>
        /// Предложить решение к выбранной задаче
        /// </summary>
        /// <returns> </returns>
        ///<remarks>
        ///Пример запроса:
        ///
        ///    {
        ///        "userId": 2,
        ///        "courseId": 1,
        ///        "taskId": 1,
        ///        "answer": "Ответ на задачу №4"
        ///    }
        ///
        /// </remarks>
        /// <response code="200">Верный ответ на задачу</response>
        /// <response code="400">Неверный ответ на задачу</response>
        /// <param name="solveTaskRequest"></param>
        [HttpPost("try-solve")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> TrySolveTaskAsync([FromBody] SolveTaskRequest solveTaskRequest)
        {
            var solved = await _taskService.TrySolveAsync(solveTaskRequest);

            if (!solved) return BadRequest("Неправильный ответ на задачу.");

            await _publishEndpoint.Publish<TaskSolved>(new
            {
                solveTaskRequest.UserId,
                solveTaskRequest.CourseId,
                solveTaskRequest.TaskId
            });
            return Ok();
        }

        /// <summary>
        /// Обновить задачу
        /// </summary>
        /// <param name="request">Данные задачи</param>
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateAsync(UpdateTaskRequest request)
        {
            await _taskService.UpdateAsync(request);
            return Ok();
        }

        /// <summary>
        /// Удалить задачу
        /// </summary>
        /// <param name="id">Идентификатор задачи</param>
        [HttpDelete("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var t = await _taskService.GetByIdAsync(id);
            await _taskService.RemoveAsync(id);

            await _publishEndpoint.Publish<TaskRemoved>(new
            {
                TaskId = id,
                CourseId = t.CourseId
            });

            return NoContent();
        }
    }
}