﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    /// Курсы
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseService _courseService;
        private readonly IIdentityService _identityService;
        private readonly IUserService _userService;

        /// <summary>
        /// Конструктор
        /// </summary>
        public CoursesController(ICourseService courseService, IIdentityService identityService, IUserService userService)
        {
            _courseService = courseService;
            _identityService = identityService;
            _userService = userService;
        }

        /// <summary>
        /// Получить все курсы
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CourseShortResponse>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CourseShortResponse>>> GetAll()
        {
            var courses = await _courseService.GetAllAsync();
            return Ok(courses);
        }

        /// <summary>
        /// Получить выбранный курс
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор курса</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CourseResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CourseResponse>> GetByIdAsync(int id)
        {
            var entity = await _courseService.GetByIdAsync(id);
            return Ok(entity);
        }

        /// <summary>
        /// Получить список курсов выбранного пользователя (создателя курсов)
        /// </summary>
        /// <returns></returns>
        /// <param name="userId">Идентификатор пользователя</param>
        [HttpGet("withowner/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<CourseShortResponse>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CourseShortResponse>>> GetByOwnerUserId(int userId)
        {
            var courses = await _courseService.GetByOwnerUserIdAsync(userId);
            if (courses is null)
            {
                return NotFound($"У пользователя с Id = {userId} нет курсов");
            }

            return Ok(courses);
        }

        /// <summary>
        /// Получить список курсов выбранного пользователя
        /// </summary>
        /// <returns></returns>
        /// <param name="userId">Идентификатор пользователя</param>
        [HttpGet("users/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<CourseShortResponse>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CourseShortResponse>>> GetWithUserId(int userId)
        {
            var courses = await _courseService.GetWithUserIdAsync(userId);
            if (courses is null)
            {
                return NotFound($"У пользователя с Id = {userId} нет курсов");
            }

            return Ok(courses);
        }

        /// <summary>
        /// Обновить данные курса
        /// </summary>
        /// <returns></returns>
        /// <param name="courseDto">Курс для изменения</param>
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateCourseRequest courseDto)
        {
            var userIdentity = _identityService.GetUserIdentity();
            var user = await _userService.GetByIdentityAsync(Guid.Parse(userIdentity));
            courseDto.UserId = user.Id;
            await _courseService.UpdateAsync(courseDto);
            return Ok();
        }

        /// <summary>
        /// Создать черновик курса
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        /// </remarks>
        /// <returns></returns>
        /// <param name="courseDto">Подробный класс курса</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateCourseDraftAsync([FromBody] CourseDraftRequest courseDto)
        {
            var courseId = await _courseService.CreateDraftAsync(courseDto);
            return CreatedAtAction(nameof(GetByIdAsync), new {id = courseId}, null);
        }

        /// <summary>
        /// Опубликовать черновик курса
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор курса</param>
        [HttpPut("{id}/publish")]
        [ProducesResponseType((int) HttpStatusCode.Accepted)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PublishCourseAsync([FromRoute] int id)
        {
            await _courseService.PublishCourseAsync(id);
            return Accepted();
        }

        /// <summary>
        /// Удалить курс
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор курса</param>
        [HttpDelete("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await _courseService.DeleteAsync(id);
            return NoContent();
        }
    }
}