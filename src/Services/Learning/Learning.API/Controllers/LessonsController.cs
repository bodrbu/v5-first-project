﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    ///  Занятия
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LessonsController : ControllerBase
    {
        private readonly ILessonService _lessonService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="lessonService"></param>
        public LessonsController(ILessonService lessonService)
        {
            _lessonService = lessonService;
        }

        /// <summary>
        /// Получить все уроки выбранного курса
        /// </summary>
        /// <param name="courseId">Идентификатор курса</param>
        /// <returns></returns>
        [HttpGet("course/{courseId}")]
        [ProducesResponseType(typeof(IEnumerable<LessonShortResponse>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<ActionResult<IEnumerable<LessonShortResponse>>> GetCourseLessonsAsync(int courseId)
        {
            var lessons = await _lessonService.GetCourseLessonsAsync(courseId);
            return Ok(lessons);
        }

        /// <summary>
        /// Получить выбранный урок
        /// </summary>
        /// <param name="id">Идентификатор урока</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LessonResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<ActionResult<LessonResponse>> GetByIdAsync(int id)
        {
            var lesson = await _lessonService.GetByIdAsync(id);
            return Ok(lesson);
        }

        /// <summary>
        /// Добавить занятие (урок) к курсу
        /// </summary>
        /// <returns></returns>
        /// <param name="createLessonRequest">Занятие</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddLessonAsync([FromBody] CreateLessonRequest createLessonRequest)
        {
            var lessonId = await _lessonService.AddLessonAsync(createLessonRequest);
            return CreatedAtAction(nameof(GetByIdAsync), new {id = lessonId}, null);
        }

        /// <summary>
        /// Изменить занятие
        /// </summary>
        /// <returns></returns>
        /// <param name="updateLessonRequest">Занятие</param>
        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditLessonAsync([FromBody] UpdateLessonRequest updateLessonRequest)
        {
            await _lessonService.UpdateLessonAsync(updateLessonRequest);
            return Accepted();
        }

        /// <summary>
        /// Удалить занятие
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> RemoveLessonAsync(int id)
        {
            await _lessonService.RemoveLessonAsync(id);
            return NoContent();
        }


    }
}