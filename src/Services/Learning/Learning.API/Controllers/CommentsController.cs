﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    [ApiController]
    [Route("api/v1/comments")]
    public class CommentsController: ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        
        /// <summary>
        /// Получить выбранный комментарий
        /// </summary>
        /// <param name="id">Идентификатор комментария</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CommentResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CommentResponse>> GetByIdAsync(int id)
        {
            var comments = await _commentService.GetAsync(id);
            return Ok(comments);
        }
        
        /// <summary>
        /// Получить комментарии для выбранноко урока
        /// </summary>
        /// <param name="id">Идентификатор урока</param>
        /// <returns></returns>
        [HttpGet("lesson/{id}")]
        [ProducesResponseType(typeof(IEnumerable<CommentResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IEnumerable<CommentResponse>>> GetLessonCommentsAsync(int id)
        {
            var comments = await _commentService.GetLessonCommentsIdAsync(id);
            return Ok(comments);
        }
        
        /// <summary>
        /// Добавить комментарий к уроку
        /// </summary>
        /// <returns></returns>
        /// <param name="commentResponse">Комментарий</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddCommentAsync([FromBody] CommentResponse commentResponse)
        {
            var commentId = await _commentService.AddCommentAsync(commentResponse);
            return CreatedAtAction(nameof(GetByIdAsync), new {id = commentId}, null);
        }
    }
}