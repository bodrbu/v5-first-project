﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Learning.API.Models.Users;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    /// Роли
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _roleService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="roleService"></param>
        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Получить все роли
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RoleDto>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<RoleDto>>> GetAllAsync()
        {
            var roles = await _roleService.GetAllAsync();
            return Ok(roles);
        }

        /// <summary>
        /// Получить данные роли по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор роли</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RoleDto), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<RoleDto>> GetByIdAsync(int id)
        {
            var role = await _roleService.GetByIdAsync(id);
            return Ok(role);
        }

        /// <summary>
        /// Обновить роль
        /// </summary>
        /// <returns></returns>
        /// <param name="role">Роль для обновления</param>
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateAsync(RoleDto role)
        {
            await _roleService.UpdateAsync(role);
            return Ok();
        }

        /// <summary>
        /// Создать роль
        /// </summary>
        /// <returns></returns>
        /// <param name="role">Роль</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddAsync(RoleDto role)
        {
            var roleId = await _roleService.AddAsync(role);
            return CreatedAtAction(nameof(GetByIdAsync), new {Id = roleId}, null);
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор роли</param>
        [HttpDelete("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _roleService.RemoveAsync(id);
            return NoContent();
        }
    }
}