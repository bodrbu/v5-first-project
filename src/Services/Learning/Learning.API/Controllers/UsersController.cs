﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EduForm.Integration.Contracts;
using Learning.API.Models.Users;
using Learning.API.Services.Abstractions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    /// Пользователи
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly ITaskService _courseService;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="sendEndpointProvider"></param>
        /// <param name="courseService"></param>
        public UsersController(IUserService userService, ISendEndpointProvider sendEndpointProvider, ITaskService courseService)
        {
            _userService = userService;
            _sendEndpointProvider = sendEndpointProvider;
            _courseService = courseService;
        }

        /// <summary>
        /// Получить всех пользователей
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserShortDto>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<UserShortDto>>> GetAllAsync()
        {
            var courses = await _userService.GetAllAsync();
            return Ok(courses);
        }

        /// <summary>
        /// Получить пользователей, у которых есть выбранынй курс
        /// </summary>
        /// <param name="courseId">Ключ к курсу</param>
        [HttpGet("withcourse/{courseId}")]
        [ProducesResponseType(typeof(IEnumerable<UserShortDto>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<UserShortDto>>> GetUsersWithCourseAsync(int courseId)
        {
            var users = await _userService.GetWithCourse(courseId);
            if (users is null)
            {
                return NotFound($"Курса с Id = {courseId} не существует");
            }

            return Ok(users);
        }

        /// <summary>
        /// Получить курсы выбранного пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpGet("{id}/courses")]
        [ProducesResponseType(typeof(IEnumerable<UserShortDto>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<UserShortDto>>> GetUserCoursesAsync(int id)
        {
            var courses = await _userService.GetUserCoursesAsync(id);
            return Ok(courses);
        }

        /// <summary>
        /// Получить пользователей, принадлежащих выбранной организации
        /// </summary>
        /// <param name="id">Ключ к организации</param>
        [HttpGet("withorganization/{id}")]
        [ProducesResponseType(typeof(IEnumerable<UserDto>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<UserShortDto>>> GetUsersByOrganizationAsync(int id)
        {
            var users = await _userService.GetWithOrganizationAsync(id);
            return Ok(users);
        }

        /// <summary>
        /// Получить данные пользователя по id
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Ключ к пользователю</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserDto), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDto>> GetByIdAsync(int id)
        {
            var entity = await _userService.GetByIdAsync(id);
            return Ok(entity);
        }

        /// <summary>
        /// Получить данные пользователя по его identity
        /// </summary>
        /// <returns></returns>
        /// <param name="identityGuid">Identity пользователя</param>
        [HttpGet("by-identity/{identityGuid}")]
        [ProducesResponseType(typeof(UserDto), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDto>> GetByIdentityAsync(string identityGuid)
        {
            var entity = await _userService.GetByIdentityAsync(Guid.Parse(identityGuid));
            return Ok(entity);
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <param name="dto">Параметры пользователя</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateUserAsync(UserDto dto)
        {
            var userId = await _userService.CreateAsync(dto);
            return CreatedAtAction(nameof(GetByIdAsync), new {id = userId}, null);
        }

        /// <summary>
        /// Обновить данные пользователя
        /// </summary>
        /// <param name="dto">Параметры пользователя</param>
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAsync(UserDto dto)
        {
            await _userService.UpdateAsync(dto);
            return Ok();
        }

        /// <summary>
        /// Включить пользователя в организацию
        /// </summary>
        /// <returns></returns>
        /// <param name="userId">Ключ к пользователю</param>
        /// <param name="orgId">Ключ к организации</param>
        [HttpPost("{userId}/organizations/{orgId}")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddOrgToUserAsync(int userId, int orgId)
        {
            await _userService.AddToOrgAsync(userId, orgId);
            return Ok();
        }

        /// <summary>
        /// Исключить пользователя из организации
        /// </summary>
        /// <returns></returns>
        /// <param name="userId">Ключ к пользователю</param>
        /// <param name="orgId">Ключ к организации</param>
        [HttpDelete("{userId}/organizations/{orgId}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DelOrgToUserAsync(int userId, int orgId)
        {
            await _userService.RemoveFromOrgAsync(userId, orgId);
            return NoContent();
        }

        /// <summary>
        /// Записать пользователя на курс
        /// </summary>
        /// <returns></returns>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="courseId">Идентификатор курса</param>
        [HttpPost("{userId}/courses/{courseId}")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> AddCourseToUserAsync(int userId, int courseId)
        {
            await _userService.AddToCourseAsync(userId, courseId);

            var tasks = await _courseService.GetHomeworksByCourseAsync(courseId);
            var ids = tasks.Select(dto => dto.Id).ToList();

            //Вызываем команду создания записей об успеваемости пользователя
            var sendEndpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("queue:performance-service"));
            await sendEndpoint.Send<AddUserPerformance>(new
            {
                UserId = userId,
                CourseId = courseId,
                TaskCount = ids.Count,
                TaskIds = ids
            });

            return Ok();
        }

        /// <summary>
        /// Отписать пользователя от курса
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="courseId">Идентификатор курса</param>
        [HttpDelete("{id}/courses/{courseId}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DelCourseToUserAsync(int id, int courseId)
        {
            await _userService.RemoveFromCourseAsync(id, courseId);
            return NoContent();
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpDelete("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _userService.DeleteAsync(id);
            return NoContent();
        }
    }
}