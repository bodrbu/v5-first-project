﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Learning.API.Models.Courses;
using Learning.API.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Learning.API.Controllers
{
    /// <summary>
    /// Организации
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsController : ControllerBase
    {
        private readonly IOrganizationService _service;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="service"></param>
        public OrganizationsController(IOrganizationService service)
        {
            _service = service;
        }

        /// <summary>
        /// Получить все организации
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<OrganizationResponse>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<OrganizationResponse>>> GetAllAsync()
        {
            var orgs = await _service.GetAllAsync();
            return Ok(orgs);
        }

        /// <summary>
        /// Получить список курсов выбранной организации
        /// </summary>
        /// <returns></returns>
        /// <param name="organizationId">Идентификатор организации</param>
        [HttpGet("{id}/courses")]
        [ProducesResponseType(typeof(IEnumerable<CourseShortResponse>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CourseShortResponse>>> GetByOrganizationId(int organizationId)
        {
            var courses = await _service.GetOrganizationCoursesAsync(organizationId);

            if (courses is null)
            {
                return NotFound($"Организации с Id = {organizationId} не существует");
            }

            return Ok(courses);
        }

        /// <summary>
        /// Получить выбранную организацию
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Идентификатор организации</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(OrganizationResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<OrganizationResponse>> GetByIdAsync(int id)
        {
            var org = await _service.GetByIdAsync(id);
            return Ok(org);
        }

        /// <summary>
        /// Получить организацию по курсу
        /// </summary>
        /// <returns></returns>
        /// <param name="courseId">Идентификатор курса</param>
        [HttpGet("withcourse/{courseId}")]
        [ProducesResponseType(typeof(OrganizationResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<OrganizationResponse>> GetByCourseIdAsync(int courseId)
        {
            var org = await _service.GetByCourseIdAsync(courseId);
            return Ok(org);
        }

        /// <summary>
        /// Обновить данные организации
        /// </summary>
        /// <returns></returns>
        /// <param name="organizationResponse">Параметры организации</param>
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> UpdateAsync(OrganizationResponse organizationResponse)
        {
            await _service.UpdateAsync(organizationResponse);
            return Ok();
        }

        /// <summary>
        /// Создать организацию
        /// </summary>
        /// <returns></returns>
        /// <param name="organization">Организация</param>
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddAsync(OrganizationResponse organization)
        {
            var orgId = await _service.AddAsync(organization);
            return CreatedAtAction(nameof(GetByIdAsync), new {Id = orgId}, null);
        }

        /// <summary>
        /// Удалить организацию
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType((int) HttpStatusCode.NoContent)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.RemoveAsync(id);
            return NoContent();
        }
    }
}