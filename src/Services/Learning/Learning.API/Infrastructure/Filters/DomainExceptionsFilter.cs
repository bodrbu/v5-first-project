﻿using System.Diagnostics;
using Core.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Learning.API.Infrastructure.Filters
{
    public class DomainExceptionsFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var traceId = Activity.Current?.Id ?? context.HttpContext?.TraceIdentifier;
            
            if (context.Exception is EntityNotFoundDomainException notFoundDomainException)
            {
                var notFoundProblemDetails = new ProblemDetails
                {
                    Status = StatusCodes.Status404NotFound,
                    Title = "Сущность не найдена",
                    Detail = notFoundDomainException.Message,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.4"
                };

                if (traceId != null)
                {
                    notFoundProblemDetails.Extensions["traceId"] = traceId;
                }

                context.Result = new NotFoundObjectResult(notFoundProblemDetails);
                context.ExceptionHandled = true;
            }
            else if (context.Exception is EduFormException eduFormException)
            {
                var problemDetails = new ProblemDetails
                {
                    Status = StatusCodes.Status400BadRequest,
                    Title = "Некорректный запрос",
                    Detail = eduFormException.Message,
                    Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1"
                };

                if (traceId != null)
                {
                    problemDetails.Extensions["traceId"] = traceId;
                }

                context.Result = new BadRequestObjectResult(problemDetails);
                context.ExceptionHandled = true;
            }
        }
    }
}