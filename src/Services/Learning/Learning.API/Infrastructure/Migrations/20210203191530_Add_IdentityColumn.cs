﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Learning.API.Infrastructure.Migrations
{
    public partial class Add_IdentityColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "IdentityGuid",
                table: "Users",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdentityGuid",
                table: "Users");
        }
    }
}
