﻿using System.Collections.Generic;

namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Урок курса
    /// </summary>
    public class LessonResponse
    {
        /// <summary>
        /// Идентификатор урока
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название урока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание к уроку
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на видео для курса
        /// </summary>
        public string VideoUri{ get; set; }

        /// <summary>
        /// ID курса, которому принадлежит урок
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Комментарии к уроку
        /// </summary>
        public IEnumerable<CommentResponse> Comments { get; set; }
    }
}
