﻿using System.Collections.Generic;
using Learning.API.Models.HomeWorks;

namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Курс
    /// </summary>
    public class CourseResponse
    {
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование курса</summary>
        public string Name { get; set; }

        ///<summary>Описание курса</summary>
        public string Description { get; set; }

        ///<summary>Организация, к которой относится курс</summary>
        public OrganizationResponse OrganizationOwner { get; set; }

        ///<summary>Пользователь, который создал курс</summary>
        public int UserId { get; set; }

        /// <summary>
        /// Список уроков курса
        /// </summary>
        public IEnumerable<LessonShortResponse> Lessons { get; set; } = new List<LessonShortResponse>();

        ///<summary>Список домашних заданий данного курса</summary>
        public IEnumerable<TaskShortDto> Tasks { get; set; } = new List<TaskShortDto>();

        /// <summary>
        /// Является ли курс черновиком
        /// </summary>
        public bool isDraft { get; set; }
    }
}
