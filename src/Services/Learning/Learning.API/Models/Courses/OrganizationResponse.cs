﻿namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Организация
    /// </summary>
    public class OrganizationResponse
    {
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int Id { get; set; }
        ///<summary>Наименование организации</summary>
        public string Name { get; set; }

        ///<summary>Описание организации</summary>
        public string Description { get; set; }
    }
}
