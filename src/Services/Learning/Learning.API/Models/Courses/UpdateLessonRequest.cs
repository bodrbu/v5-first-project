﻿namespace Learning.API.Models.Courses
{
    public class UpdateLessonRequest
    {
        /// <summary>
        /// Идентификатор урока
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Название урока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание к уроку
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на видео для курса
        /// </summary>
        public string VideoUri { get; set; }
    }
}