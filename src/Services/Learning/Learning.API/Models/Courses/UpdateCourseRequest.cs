﻿namespace Learning.API.Models.Courses
{
    public class UpdateCourseRequest
    {
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование курса</summary>
        public string Name { get; set; }

        ///<summary>Описание курса</summary>
        public string Description { get; set; }

        ///<summary>Организация, к которой относится курс</summary>
        public int OrganizationId { get; set; }

        ///<summary>Пользователь, который создал курс</summary>
        public int UserId { get; set; }
    }
}