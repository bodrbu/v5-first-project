﻿namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Упрощённая модель курса
    /// </summary>
    public class CourseShortResponse
    {
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование курса</summary>
        public string Name { get; set; }

        ///<summary>Описание курса</summary>
        public string Description { get; set; }

        ///<summary>Ключ к организации, к которой относится курс</summary>
        public int OrganizationOwnerId { get; set; }

        ///<summary>Ключ к пользователю, который создал курс</summary>
        public int UserOwnerId { get; set; }

        /// <summary>
        /// Является ли курс черновиком
        /// </summary>
        public bool isDraft { get; set; }

    }
}
