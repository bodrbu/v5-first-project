﻿namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Комментарйи к уроку
    /// </summary>
    public class CommentResponse
    {
        /// <summary>
        /// Идентификатор комментария
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор урока
        /// </summary>
        public int LessonId { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public string CreationDate { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}