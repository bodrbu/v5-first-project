﻿namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Запрос на добавление урока к курсу
    /// </summary>
    public class CreateLessonRequest
    {
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Название урока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание к уроку
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на видео для курса
        /// </summary>
        public string VideoUri { get; set; }
    }
}