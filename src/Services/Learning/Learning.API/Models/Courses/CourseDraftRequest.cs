﻿namespace Learning.API.Models.Courses
{
    /// <summary>
    /// Команда для создания черновика курса
    /// </summary>
    public class CourseDraftRequest
    {
        ///<summary>Наименование курса</summary>
        public string Name { get; set; }

        ///<summary>Описание курса</summary>
        public string Description { get; set; }

        ///<summary>Организация, к которой относится курс</summary>
        public int OrganizationId { get; set; }

        ///<summary>Пользователь, который создал курс</summary>
        public int UserId { get; set; }
    }
}