﻿#pragma warning disable 1591
namespace Learning.API.Models.Courses
{
    public class LessonShortResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}