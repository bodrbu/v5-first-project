﻿namespace Learning.API.Models.Users
{
    /// <summary></summary>
    public class CreateOrEditUser
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }
        ///<summary>Имя пользователя</summary>
        public string Name { get; set; }

        ///<summary>Фамилия пользователя</summary>
        public string Surname { get; set; }

        ///<summary>Логин пользователя</summary>
        public string Login { get; set; }

        ///<summary>Пароль пользователя</summary>
        public string Password { get; set; }

        ///<summary>Ключ к роли пользователя</summary>
        public int RoleId { get; set; }
    }
}
