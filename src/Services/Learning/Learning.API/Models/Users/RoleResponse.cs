﻿namespace Learning.API.Models.Users
{
    /// <summary>
    /// Роль
    /// </summary>
    public class RoleDto
    {
        /// <summary>
        /// Идентификатор роли
        /// </summary>
        public int Id { get; set; }

        ///<summary>Имя роли</summary>
        public string Name { get; set; }

        ///<summary>Описание роли</summary>
        public string Description { get; set; }
    }
}