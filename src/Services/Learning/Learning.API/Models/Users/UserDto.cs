﻿using System;

namespace Learning.API.Models.Users
{
    /// <summary></summary>
    public class UserDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Guid из Identity-сервера
        /// </summary>
        public string IdentityGuid { get; set; }

        ///<summary>Имя пользователя</summary>
        public string Name { get; set; }

        ///<summary>Фамилия пользователя</summary>
        public string Surname { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronimyc { get; set; }

        ///<summary>Логин пользователя</summary>
        public string Login { get; set; }

        ///<summary>Пароль пользователя</summary>
        public string Password { get; set; }

        ///<summary>Роль пользователя</summary>
        public int RoleId { get; set; }

        /// <summary>
        /// День рождения
        /// </summary>
        public DateTime BDay { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime DateModified { get; set; }

        /// <summary>
        /// Идентификатор организации, к которой принадлежит пользователь
        /// </summary>
        public int? OrganizationId { get; set; }
    }
}