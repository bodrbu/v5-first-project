﻿namespace Learning.API.Models.Users
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class UserShortDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Guid из Identity-сервера
        /// </summary>
        public string IdentityGuid { get; set; }
    }
}