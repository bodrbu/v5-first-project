﻿
#pragma warning disable 1591

namespace Learning.API.Models.HomeWorks
{
    public class TaskShortDto
    {
        /// <summary>
        /// Идентификатор домашнего задания
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование домашнего задания</summary>
        public string Name { get; set; }
    }
}