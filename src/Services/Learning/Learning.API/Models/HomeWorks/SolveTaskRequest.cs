﻿using MassTransit.Internals.Reflection;

namespace Learning.API.Models.HomeWorks
{
    public class SolveTaskRequest
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public int TaskId { get; set; }
        public string Answer { get; set; }
    }
}