﻿using System.Collections.Generic;

namespace Learning.API.Models.HomeWorks
{
    /// <summary>
    /// Домашнее задание
    /// </summary>
    public class UpdateTaskRequest
    {
        /// <summary>
        /// Идентификатор домашнего задания
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование домашнего задания</summary>
        public string Name { get; set; }

        ///<summary>Описание домашнего задания</summary>
        public string Description { get; set; }

        /// <summary>
        /// Правильный ответ на задачу
        /// </summary>
        public string Answer { get; set; }
    }
}