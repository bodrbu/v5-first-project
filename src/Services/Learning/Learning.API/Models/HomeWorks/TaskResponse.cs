﻿// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Learning.API.Models.HomeWorks
{
    public class TaskResponse
    {
        /// <summary>
        /// Идентификатор домашнего задания
        /// </summary>
        public int Id { get; set; }

        ///<summary>Наименование домашнего задания</summary>
        public string Name { get; set; }

        ///<summary>Описание домашнего задания</summary>
        public string Description { get; set; }

        ///<summary>Ключ к курсу, для которого создано домашнее задание</summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Ответ на задачу
        /// </summary>
        public string Answer { get; set; }
    }
}