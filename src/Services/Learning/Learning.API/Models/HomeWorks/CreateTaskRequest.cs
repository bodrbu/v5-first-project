﻿// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Learning.API.Models.HomeWorks
{
    /// <summary>
    /// Домашнее задание
    /// </summary>
    public class CreateTaskRequest
    {

        ///<summary>Наименование задачи</summary>
        public string Name { get; set; }

        ///<summary>Описание задачи</summary>
        public string Description { get; set; }

        ///<summary>Ключ к курсу, для которого создано задание</summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Правильный ответ на задачу
        /// </summary>
        public string Answer { get; set; }
    }
}