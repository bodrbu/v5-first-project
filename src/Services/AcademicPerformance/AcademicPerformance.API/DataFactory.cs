﻿using System;
using System.Collections.Generic;
using AcademicPerformance.API.Model;

namespace AcademicPerformance.API
{
    public static class DataFactory
    {
        public static List<PerformanceSummary> FPerformanceSummaries => new List<PerformanceSummary>
        {
            new PerformanceSummary()
            {
                CourseId = 1,
                UserId = 1,
                TaskCount = 2,
                SolvedTaskCount = 1,
                Details = new List<PerformanceDetails>()
                {
                    new PerformanceDetails
                    {
                        TaskId = 3,
                        IsSolved = true,
                        SolvedDate = DateTime.UtcNow
                    },
                    new PerformanceDetails
                    {
                        TaskId = 4,
                        IsSolved = false,
                        SolvedDate = null
                    }
                }
            },
            new PerformanceSummary()
            {
                CourseId = 1,
                UserId = 2,
                TaskCount = 2,
                SolvedTaskCount = 0,
                Details = new List<PerformanceDetails>
                {
                    new PerformanceDetails
                    {
                        TaskId = 3,
                        IsSolved = false,
                        SolvedDate = null
                    },
                    new PerformanceDetails
                    {
                        TaskId = 4,
                        IsSolved = false,
                        SolvedDate = null
                    }
                }
            },
            new PerformanceSummary()
            {
                CourseId = 2,
                UserId = 1,
                TaskCount = 2,
                SolvedTaskCount = 2,
                Details = new List<PerformanceDetails>()
                {
                    new PerformanceDetails
                    {
                        TaskId = 1,
                        IsSolved = true,
                        SolvedDate = DateTime.Now
                    },
                    new PerformanceDetails
                    {
                        TaskId = 2,
                        IsSolved = true,
                        SolvedDate = DateTime.Now.Subtract(TimeSpan.FromDays(1))
                    }
                }
            }
        };
    }
}