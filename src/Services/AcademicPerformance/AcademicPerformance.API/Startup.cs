using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using AcademicPerformance.API.Infrastructure;
using AcademicPerformance.API.Integration.Contracts.Consumers;
using AcademicPerformance.API.Model;
using AcademicPerformance.API.Options;
using FluentValidation.AspNetCore;
using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag;
using NSwag.AspNetCore;
using NSwag.Generation.Processors.Security;
using Serilog;

namespace AcademicPerformance.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options => { options.SuppressAsyncSuffixInActionNames = false; })
                .AddFluentValidation(fv => { fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()); });

            services.AddNSwag(Configuration);

            services.AddCustomAuthentication(Configuration);
            services.AddCustomAuthorization(Configuration);
            services.AddDbContext<PerformanceDbContext>(x =>
            {
                x.UseNpgsql(Configuration.GetValue<string>("ConnectionString"),
                    option => { option.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName); });
                if (Debugger.IsAttached)
                {
                    x.EnableSensitiveDataLogging();
                }
            });

            services.AddScoped<IInitialize, EfDbInitializer>();
            services.AddMassTransitEndpoints(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IInitialize dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else if (env.IsProduction())
            {
                app.UseExceptionHandler("/error");
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
                x.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "performanceswaggerui",
                    AppName = "Performance Swagger UI"
                };
            });

            app.UseCors(options =>
                options.AllowAnyOrigin().AllowAnyMethod()
                    .AllowAnyHeader()); //.WithOrigins(Configuration.GetValue<string>("SpaHostUrl")));

            app.UseRouting();
            app.UseSerilogRequestLogging();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization("ApiScope");
            });

            dbInitializer.Initialize();
        }
    }

    public static class ServicesExtensions
    {
        /// <summary>
        /// Добавляет и настраивает шину сообщений с помощью автоименования эндпоинтов
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IServiceCollection AddMassTransitEndpoints(this IServiceCollection services,
            IConfiguration config)
        {
            var opts = config.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<AddUserPerformanceConsumer>();
                x.AddConsumer<TaskSolvedConsumer>();
                x.AddConsumer<TaskAddedConsumer>();
                x.AddConsumer<TaskRemovedConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.UseMessageRetry(r =>
                    {
                        r.Immediate(3);
                        r.Handle<SqlException>();
                    });

                    cfg.Host(opts.Host, opts.Port, opts.VirtualHost, h =>
                    {
                        h.Username("guest");
                        h.Password("guest");
                    });

                    cfg.ReceiveEndpoint("academic-performance-service",
                        configurator => { configurator.ConfigureConsumers(context); });
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }

        public static IServiceCollection AddNSwag(this IServiceCollection services, IConfiguration configuration)
        {
            var identityExternalUrl = configuration.GetValue<string>("IdentityExternalUrl");
            services.AddOpenApiDocument(options =>
            {
                options.Title = "EduForm.AcademicPerformance API";
                options.Version = "1.0";
                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = $"{identityExternalUrl}/connect/authorize",
                            TokenUrl = $"{identityExternalUrl}/connect/token",
                            Scopes = new Dictionary<string, string>
                            {
                                {"performance", "Performance Service"}
                            }
                        }
                    }
                });
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });
            return services;
        }

        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");

            var identityUrl = configuration.GetValue<string>("IdentityUrl");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = identityUrl;
                options.RequireHttpsMetadata = false;
                options.Audience = "performance";
            });

            return services;
        }

        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", builder =>
                {
                    builder.RequireAuthenticatedUser();
                    builder.RequireClaim("scope", "performance");
                });
            });
            return services;
        }
    }
}