﻿namespace AcademicPerformance.API.Dto
{
    /// <summary>
    /// Общая информация об успеваемости пользователя на курсе
    /// </summary>
    public class PerformanceSummaryResponse
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// Количество задач на курсе
        /// </summary>
        public int TaskCount { get; set; }
        /// <summary>
        /// Количество решённых задач на курсе
        /// </summary>
        public int SolvedTaskCount { get; set; }
    }
}