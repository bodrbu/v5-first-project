﻿using System;

namespace AcademicPerformance.API.Dto
{
    /// <summary>
    /// Детальная информация о задаче
    /// </summary>
    public class DetailsDto
    {
        /// <summary>
        /// Идентификатор задачи
        /// </summary>
        public int TaskId { get; set; }
        /// <summary>
        /// Является ли задача решённой
        /// </summary>
        public bool IsSolved { get; set; }
        /// <summary>
        /// Время решения задачи
        /// </summary>
        public DateTime? SolvedDate { get; set; }
    }
}