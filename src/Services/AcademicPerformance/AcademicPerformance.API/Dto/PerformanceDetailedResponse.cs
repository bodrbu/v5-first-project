﻿using System.Collections.Generic;

namespace AcademicPerformance.API.Dto
{
    /// <summary>
    /// Детальная информация об успеваемости пользователя на курсе
    /// </summary>
    public class PerformanceDetailedResponse
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public int CourseId { get; set; }
        /// <summary>
        /// Количество задач на курсе
        /// </summary>
        public int TaskCount { get; set; }
        /// <summary>
        /// Количество решённых задач на курсе
        /// </summary>
        public int SolvedTaskCount { get; set; }
        /// <summary>
        /// Детальная информация о задачах
        /// </summary>
        public List<DetailsDto> Details { get; set; }
    }
}