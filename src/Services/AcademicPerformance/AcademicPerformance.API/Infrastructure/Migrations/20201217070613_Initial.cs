﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AcademicPerformance.API.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PerformanceSummary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false),
                    TaskCount = table.Column<int>(nullable: false),
                    SolvedTaskCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerformanceSummary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PerformanceDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DateCreate = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    SummaryId = table.Column<int>(nullable: false),
                    TaskId = table.Column<int>(nullable: false),
                    IsSolved = table.Column<bool>(nullable: false),
                    SolvedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerformanceDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PerformanceDetails_PerformanceSummary_SummaryId",
                        column: x => x.SummaryId,
                        principalTable: "PerformanceSummary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PerformanceDetails_SummaryId",
                table: "PerformanceDetails",
                column: "SummaryId");

            migrationBuilder.CreateIndex(
                name: "IX_PerformanceSummary_CourseId",
                table: "PerformanceSummary",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_PerformanceSummary_UserId",
                table: "PerformanceSummary",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PerformanceDetails");

            migrationBuilder.DropTable(
                name: "PerformanceSummary");
        }
    }
}
