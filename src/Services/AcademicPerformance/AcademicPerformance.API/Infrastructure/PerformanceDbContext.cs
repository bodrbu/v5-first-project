﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AcademicPerformance.API.Model;
using Microsoft.EntityFrameworkCore;

namespace AcademicPerformance.API.Infrastructure
{
    public class PerformanceDbContext : DbContext
    {
        public PerformanceDbContext(DbContextOptions<PerformanceDbContext> dbContextOptions): base(dbContextOptions)
        {
        }

        public DbSet<PerformanceSummary> PerformanceSummary => Set<PerformanceSummary>();
        public DbSet<PerformanceDetails> PerformanceDetails => Set<PerformanceDetails>();
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PerformanceSummary>()
                .HasIndex(summary => summary.CourseId);
            modelBuilder.Entity<PerformanceSummary>()
                .HasIndex(summary => summary.UserId);
            modelBuilder.Entity<PerformanceSummary>().HasMany(summary => summary.Details)
                .WithOne(details => details.Summary).IsRequired(true);
        }
        
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var entries = ChangeTracker.Entries<BaseEntity>()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified);

            foreach (var entry in entries)
            {
                entry.Entity.DateModified = DateTime.Now;
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreate = DateTime.Now;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
        
        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries<BaseEntity>()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified);

            foreach (var entry in entries)
            {
                entry.Entity.DateModified = DateTime.Now;
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreate = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}