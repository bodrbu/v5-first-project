﻿using AcademicPerformance.API.Model;
using Microsoft.EntityFrameworkCore;

namespace AcademicPerformance.API.Infrastructure
{
    public class EfDbInitializer : IInitialize
    {
        private readonly PerformanceDbContext _dataContext;

        public EfDbInitializer(PerformanceDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.Migrate();
            
            _dataContext.AddRange(DataFactory.FPerformanceSummaries);
            _dataContext.SaveChanges();
        }
    }
}