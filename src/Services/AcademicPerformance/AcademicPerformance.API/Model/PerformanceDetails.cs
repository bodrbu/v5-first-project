﻿using System;

namespace AcademicPerformance.API.Model
{
    public class PerformanceDetails : BaseEntity
    {
        public PerformanceSummary Summary { get; set; }
        public int SummaryId { get; set; }

        public int TaskId { get; set; }
        public bool IsSolved { get; set; }
        public DateTime? SolvedDate { get; set; }
    }
}