﻿using System.Collections.Generic;

namespace AcademicPerformance.API.Model
{
    /// <summary>
    /// Успеваемость (количество решённых заданий) студента на курсе
    /// </summary>
    public class PerformanceSummary: BaseEntity
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        
        public int TaskCount { get; set; }
        
        public int SolvedTaskCount { get; set; }

        public ICollection<PerformanceDetails> Details { get; set; }
    }
}