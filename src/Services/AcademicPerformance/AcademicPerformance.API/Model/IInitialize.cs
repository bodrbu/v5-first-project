﻿namespace AcademicPerformance.API.Model
{
    public interface IInitialize
    {
        public void Initialize();
    }
}
