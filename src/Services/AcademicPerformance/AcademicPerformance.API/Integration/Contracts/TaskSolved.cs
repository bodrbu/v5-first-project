﻿// ReSharper disable CheckNamespace

namespace EduForm.Integration.Contracts
{
    public interface TaskSolved
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public int TaskId { get; set; }
    }
}