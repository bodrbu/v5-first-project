﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcademicPerformance.API.Infrastructure;
using AcademicPerformance.API.Model;
using EduForm.Integration.Contracts;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AcademicPerformance.API.Integration.Contracts.Consumers
{
    public class TaskSolvedConsumer : IConsumer<TaskSolved>
    {
        private readonly ILogger<AddUserPerformanceConsumer> _logger;
        private readonly PerformanceDbContext _context;

        public TaskSolvedConsumer(ILogger<AddUserPerformanceConsumer> logger, PerformanceDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<TaskSolved> context)
        {
            _logger.LogInformation("Start consuming message with Id = {messageId} - ({@message})", context.MessageId,
                context.Message);

            var perfSummary = await _context.PerformanceSummary
                .Include(summary => summary.Details)
                .SingleOrDefaultAsync(s =>
                    s.CourseId == context.Message.CourseId && s.UserId == context.Message.UserId);

            var performanceDetails = perfSummary.Details.First(details => details.TaskId == context.Message.TaskId);

            if (!performanceDetails.IsSolved)
            {
                performanceDetails.IsSolved = true;
                performanceDetails.SolvedDate = DateTime.UtcNow;

                ++perfSummary.SolvedTaskCount;

                await _context.SaveChangesAsync();
            }

            _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                context.MessageId, context.Message);
        }
    }
}