﻿using System.Linq;
using System.Threading.Tasks;
using AcademicPerformance.API.Infrastructure;
using EduForm.Integration.Contracts;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AcademicPerformance.API.Integration.Contracts.Consumers
{
    public class TaskRemovedConsumer : IConsumer<TaskRemoved>
    {
        private readonly ILogger<TaskRemovedConsumer> _logger;
        private readonly PerformanceDbContext _dbContext;

        public TaskRemovedConsumer(ILogger<TaskRemovedConsumer> logger, PerformanceDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<TaskRemoved> context)
        {
            _logger.LogInformation("Start consuming message with Id = {messageId} - ({@message})", context.MessageId,
                context.Message);

            var details = await _dbContext.PerformanceDetails
                .Include(d => d.Summary)
                .Where(d => d.TaskId == context.Message.TaskId)
                .ToListAsync();

            if (details.Count == 0)
            {
                _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                    context.MessageId, context.Message);
                return;
            }

            foreach (var performanceDetails in details)
            {
                performanceDetails.Summary.TaskCount--;
            }

            _dbContext.RemoveRange(details);
            await _dbContext.SaveChangesAsync();

            _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                context.MessageId, context.Message);
        }
    }
}