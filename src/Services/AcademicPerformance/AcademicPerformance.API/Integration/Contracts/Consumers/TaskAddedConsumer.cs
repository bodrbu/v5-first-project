﻿using System.Linq;
using System.Threading.Tasks;
using AcademicPerformance.API.Infrastructure;
using AcademicPerformance.API.Model;
using EduForm.Integration.Contracts;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AcademicPerformance.API.Integration.Contracts.Consumers
{
    public class TaskAddedConsumer : IConsumer<TaskAdded>
    {
        private readonly ILogger<TaskAddedConsumer> _logger;
        private readonly PerformanceDbContext _dbContext;

        public TaskAddedConsumer(ILogger<TaskAddedConsumer> logger, PerformanceDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<TaskAdded> context)
        {
            _logger.LogInformation("Start consuming message with Id = {messageId} - ({@message})", context.MessageId,
                context.Message);

            var summaries = await _dbContext.PerformanceSummary
                .Include(s => s.Details)
                .Where(s => s.CourseId == context.Message.CourseId)
                .ToListAsync();

            if (summaries.Count == 0)
            {
                _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                    context.MessageId, context.Message);
                return;
            }

            foreach (var performanceSummary in summaries)
            {
                performanceSummary.TaskCount++;
                performanceSummary.Details.Add(new PerformanceDetails
                {
                    IsSolved = false,
                    TaskId = context.Message.TaskId
                });
            }

            _dbContext.UpdateRange(summaries);
            await _dbContext.SaveChangesAsync();

            _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                context.MessageId, context.Message);
        }
    }
}