﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AcademicPerformance.API.Infrastructure;
using AcademicPerformance.API.Model;
using EduForm.Integration.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace AcademicPerformance.API.Integration.Contracts.Consumers
{
    public class AddUserPerformanceConsumer : IConsumer<AddUserPerformance>
    {
        private readonly ILogger<AddUserPerformanceConsumer> _logger;
        private readonly PerformanceDbContext _context;

        public AddUserPerformanceConsumer(ILogger<AddUserPerformanceConsumer> logger, PerformanceDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task Consume(ConsumeContext<AddUserPerformance> context)
        {
            _logger.LogInformation("Start consuming message with Id = {messageId} - ({@message})", context.MessageId,
                context.Message);

            var perfSummary = new PerformanceSummary
            {
                CourseId = context.Message.CourseId,
                TaskCount = context.Message.TaskCount,
                UserId = context.Message.UserId,
                Details = new List<PerformanceDetails>(context.Message.TaskCount)
            };
            foreach (var taskId in context.Message.TaskIds)
            {
                perfSummary.Details.Add(new PerformanceDetails
                {
                    IsSolved = false,
                    TaskId = taskId
                });
            }

            await _context.AddAsync(perfSummary);
            await _context.SaveChangesAsync();
            _logger.LogInformation("Message with Id = {messageId} has been consumed successfully - ({@message})",
                context.MessageId, context.Message);
        }
    }
}