﻿using System;
using System.Collections.Generic;

// ReSharper disable CheckNamespace

namespace EduForm.Integration.Contracts
{
    public interface AddUserPerformance
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        
        public int TaskCount { get; set; }

        public List<int> TaskIds { get; set; }
    }
}