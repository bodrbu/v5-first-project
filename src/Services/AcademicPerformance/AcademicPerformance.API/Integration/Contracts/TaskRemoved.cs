﻿// ReSharper disable CheckNamespace

namespace EduForm.Integration.Contracts
{
    public interface TaskRemoved
    {
        public int CourseId { get; set; }
        public int TaskId { get; set; }
    }
}