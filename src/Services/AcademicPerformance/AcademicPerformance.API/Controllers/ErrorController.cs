﻿using Microsoft.AspNetCore.Mvc;

namespace AcademicPerformance.API.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public IActionResult Error() => Problem();
    }
}