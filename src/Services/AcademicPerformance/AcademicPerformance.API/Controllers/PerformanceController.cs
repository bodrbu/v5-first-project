﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AcademicPerformance.API.Dto;
using AcademicPerformance.API.Infrastructure;
using AcademicPerformance.API.Model;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AcademicPerformance.API.Controllers
{
    [ApiController]
    [Route("api/v1/performance/")]
    public class PerformanceController : ControllerBase
    {
        private readonly PerformanceDbContext _context;

        public PerformanceController(PerformanceDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Получить общую информацию об успеваемости пользователя на курсе
        /// </summary>
        /// <param name="courseId">Идентификатор курса</param>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     courseId = 1
        ///     userId = 2
        /// </remarks>
        [ProducesResponseType(typeof(PerformanceSummaryResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [HttpGet("summary/user/{userId}/course/{courseId}")]
        public async Task<ActionResult<PerformanceSummaryResponse>> GetPerformanceSummaryAsync(int courseId, int userId)
        {
            var summary = await _context.PerformanceSummary
                .AsNoTracking()
                .Where(s => s.CourseId == courseId && s.UserId == userId)
                .FirstOrDefaultAsync();

            if (summary is null)
            {
                return NotFound();
            }

            var res = summary.Adapt<PerformanceSummaryResponse>();

            return Ok(res);
        }

        /// <summary>
        /// Получить общую информацию об успеваемости пользователя на его курсах
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     userId = 2
        /// </remarks>
        [HttpGet("summary/user/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<PerformanceSummaryResponse>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<IEnumerable<PerformanceSummary>>> GetPerformanceSummaryByUserAsync(int userId)
        {
            var summaries = await _context.PerformanceSummary
                .AsNoTracking()
                .Where(s => s.UserId == userId)
                .ToListAsync();

            if (summaries is null || summaries.Count == 0)
            {
                return NotFound();
            }

            var resList = summaries.Select(s => s.Adapt<PerformanceSummaryResponse>());
            return Ok(resList);
        }

        /// <summary>
        /// Получить общую информацию об успеваемости пользователей на курсе
        /// </summary>
        /// <param name="courseId">Идентификатор курса</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     courseId = 1
        /// </remarks>
        [HttpGet("summary/course/{courseId}")]
        [ProducesResponseType(typeof(IEnumerable<PerformanceSummaryResponse>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<IEnumerable<PerformanceSummaryResponse>>> GetPerformanceSummaryByCourseAsync(
            int courseId)
        {
            var summaries = await _context.PerformanceSummary
                .AsNoTracking()
                .Where(s => s.CourseId == courseId)
                .ToListAsync();

            if (summaries is null || summaries.Count == 0)
            {
                return NotFound();
            }

            var resList = summaries.Select(s => s.Adapt<PerformanceSummaryResponse>());

            return Ok(resList);
        }

        /// <summary>
        /// Получить детальную информацию об успеваемости пользователя на курсе
        /// </summary>
        /// <param name="courseId">Идентификатор курса</param>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     courseId = 1
        ///     userId = 2
        /// </remarks>
        [HttpGet("details/user/{userId}/course/{courseId}")]
        [ProducesResponseType(typeof(PerformanceDetailedResponse), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<PerformanceDetailedResponse>> GetPerformanceDetailsAsync(int courseId,
            int userId)
        {
            var summary = await _context.PerformanceSummary
                .AsNoTracking()
                .Include(s => s.Details)
                .Where(s => s.CourseId == courseId && s.UserId == userId)
                .FirstOrDefaultAsync();

            if (summary is null)
            {
                return NotFound();
            }

            var res = summary.Adapt<PerformanceDetailedResponse>();

            return Ok(res);
        }

        /// <summary>
        /// Получить детальную информацию об успеваемости пользователя на его курсах
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     courseId = 1
        ///     userId = 2
        /// </remarks>
        [HttpGet("details/user/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<PerformanceDetailedResponse>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<IEnumerable<PerformanceDetailedResponse>>> GetPerformanceDetailsByUserAsync(
            int userId)
        {
            var summaries = await _context.PerformanceSummary
                .AsNoTracking()
                .Include(s => s.Details)
                .Where(s => s.UserId == userId)
                .ToListAsync();

            if (summaries is null || summaries.Count == 0)
            {
                return NotFound();
            }

            var resList = summaries.Select(s => s.Adapt<PerformanceDetailedResponse>());
            return Ok(resList);
        }

        /// <summary>
        /// Получить детальную информацию об успеваемости всех пользователей на курсе
        /// </summary>
        /// <param name="courseId">Идентификатор курса</param>
        /// <returns></returns>
        /// <remarks>
        /// Пример значений:
        ///
        ///     courseId = 1
        ///     userId = 2
        /// </remarks>
        [HttpGet("details/course/{courseId}")]
        [ProducesResponseType(typeof(IEnumerable<PerformanceDetailedResponse>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult<IEnumerable<PerformanceDetailedResponse>>> GetPerformanceDetailsByCourseAsync(
            int courseId)
        {
            var summaries = await _context.PerformanceSummary
                .AsNoTracking()
                .Include(s => s.Details)
                .Where(s => s.CourseId == courseId)
                .ToListAsync();

            if (summaries is null || summaries.Count == 0)
            {
                return NotFound();
            }

            var resList = summaries.Select(s => s.Adapt<PerformanceDetailedResponse>());
            return Ok(resList);
        }
    }
}